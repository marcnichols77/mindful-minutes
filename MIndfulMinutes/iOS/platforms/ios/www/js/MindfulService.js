﻿/* Global Variables*/

//var servicePath = "http://192.168.1.79/MindfulMinutesService/MindfulMinutesService.svc/";
//var servicePath = "http://localhost:49277/MindfulMinutesService.svc/";
var servicePath = "http://ec2-54-200-164-172.us-west-2.compute.amazonaws.com/mindfulminuteservice/MindfulMinutesService.svc/";

//Declare Varibales which are using in AJAX method.
var Type = "POST";
var Url = servicePath;
var Data = "";
var ContentType = "application/json;charset=utf-8";
var DataType = "json";
var crossDomain = true;
var isSuccess = false;
var callname = "";

//Call MindfulMinutes Service
function callMindfulService(data, callname) {

    $("#dvProcessing").show();
    Url = servicePath + callname;

    $.ajax({
        type: Type,
        url: Url,
        data: data,
        contentType: ContentType,
        dataType: DataType,
        crossDomain: crossDomain,
        success: function (result) {
            $("#dvProcessing").hide();
            if (result.Response.Result) {
                ServiceSucceeded(result, callname);
            } else {
                ServiceFailed(result);
            }
        },
        error: function (jqXHR, exception) {
            $("#dvProcessing").hide();
            //alert('Internal error: ' + jqXHR.responseText);
            
            //ServiceFailed(result);                        

            if (jqXHR.status == 0) {
                alert('Not connected.\n Verify Your Internet Connection.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found. [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (exception === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                alert('Time out error.');
            } else if (exception === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error.\n' + jqXHR.responseText);
            }
        }
    });
}

function ServiceFailed(result) {
    alert(result.Response.Message);
}

function ServiceSucceeded(result, callname) {
    switch (callname) {
        case "SignUp":
            SignUpCallback(result);
            break;
        case "SignIn":
            SignInCallback(result);
            break;
        case "GetMemberProfile":
            GetProfileCallback(result);
            break;
        case "EditProfilePicture":
            EditPictureCallback(result);
            break;
        case "EditMemberProfile":
            EditProfileCallback(result);
            break;
        case "GetUserSettings":
            GetUserSettingsCallback(result);
            break;
        case "EditUserSettings":
            SaveUserSettingsCallback(result);
            break;
        case "ForgotPassword":
            RequestPasswordCallback(result);
            break;
        case "GetAmbientSoundList":
            GetAmbientSoundCallback(result);
            break;
        case "AddPresetTimer":
            SavePresetTimerCallback(result);
            break;
        case "GetPresetTimerList":
            GetPresetTimerListCallback(result);
            break;
        case "GetPresetTimerByPresetId":
            GetPresetTimerCallback(result);
            break;
        case "DeletePresetTimer":
            DeletePresetTimerCallback(result);
            break;
        case "AddMeditationStats":
            AddMeditationStatsCallback(result);
            break;
        case "GetAllUsersLocation":
            GetAllUsersLocationCallback(result);
            break;
        case "GetNeurominutesFreeSamplerList":
            GetFreeSamplerCallback(result);
            break;
        case "IncreaseNeurominutesFreeSamplerViewCount":
            IncreaseViewCountCallback(result);
            break;
        case "GetNeurominutesTrackList":
            GetNeurominTrackCallback(result);
            break;
        case "GetNeurominutesTrackDetailListByTrackId":
            GetNeurominTrackDetailCallback(result);
            break;
        case "AddNeurominReview":
            AddNeurominReviewCallback(result);
            break;
        case "LikeUnlikeTrack":
            if ($('#iLikeUnlike').hasClass("text-success"))
                $('#iLikeUnlike').removeClass("text-success");
            else
                $('#iLikeUnlike').addClass("text-success");
            break;
        case "GetAllGuidedMeditationCategory":
            GetAllGuidedMeditationCategoryCallback(result);
            break;
        case "GetAllGuidedUploadMeditationByCategory":
            GetAllGuidedUploadMeditationByCategoryCallback(result);
            break;
        case "GetGuidedUploadMeditationDetails":
            GetGuidedUploadMeditationDetailsCallback(result);
            break;
        case "UpdatePlayCount":
            UpdatePlayCountCallback(result);
            break;
        case "AddNeurominReview":
            AddNeurominReviewCallback(result);
            break;
        case "ManageUserRating":
            ManageUserRatingCallback(result);
            break;
        case "GetUserRating":
            GetUserRatingCallback(result);
            break;
        case "GetRelatedGuidedUploadMeditation":
            GetRelatedGuidedUploadMeditationCallback(result);
            break;
        case "InsertSubscriptionForApplication":
            if (result.Response.Result) {
                var userData = JSON.parse(window.localStorage.getItem("userData"));
                userData.IsFullPurchased = true;
                window.localStorage.setItem("userData", JSON.stringify(userData));
                $('#DivFullSubscription').hide();
            }
            alert(result.Response.Message);
            $('.modal').modal('hide');
            break;
        case "InsertSubscriptionForModule":
            if (result.Response.Result)
                $('#aPricingPlan').hide();
            alert(result.Response.Message);
            $('.modal').modal('hide');
            break;
        case "InsertSubscriptionForProduct":
            //if (result.Response.Result)
            //    $('#aPricingPlan').hide();
            alert(result.Response.Message);
            $('.modal').modal('hide');
            window.location = window.location;
            break;
        case "GetAllGuidedMeditationCategoryDropDown":
            GetAllGuidedMeditationCategoryDropDownCallback(result);
            break;
        case "InsertGuidedMeditationData":
            InsertGuidedMeditationDataCallback(result);
            break;
        case "InsertUpdateMyUploads":
            InsertUpdateMyUploadsCallback(result);
            break;
        case "GetAllMyUpload":
            GetAllMyUploadCallback(result);
            break;
        case "UpdateMyUploadMeditationPlayCount":
            UpdateMyUploadMeditationPlayCountCallback(result);
            break;
        case "GetGuidedUploadMeditationMediaList":
            GetGuidedUploadMeditationMediaListCallback(result);
            break;
        case "UpdateMyUploadMeditationMediaPlayCount":
            UpdateMyUploadMeditationMediaPlayCountCallback(result);
            break;
        case "CheckForModuleSubscription":
            CheckForModuleSubscriptionCallback(result);
            break;
        case "AddGuidedMeditationStats":
            AddGuidedMeditationStatsCallback(result);
            break;
        case "AddAppReview":
            location.href = ""; //store url scheme
            break;
        default:
    }
}