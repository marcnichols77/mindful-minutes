﻿var roleId = 2,
    redirectAfterLogin = "dashboard.html",
    screenextension = ".html",
    isProfileEdit = false,
        GuidedAudioUrl = "http://ec2-54-200-164-172.us-west-2.compute.amazonaws.com/admin/AppResource/GuidedUploadMeditaion/Audio/",
        GuidedPdfUrl = "http://ec2-54-200-164-172.us-west-2.compute.amazonaws.com/admin/AppResource/GuidedUploadMeditaion/PDF/",
        TimerBgPath = "http://ec2-54-200-164-172.us-west-2.compute.amazonaws.com/MindfulMinuteService/img/timer/",
        ResourceAudioUrl = "http://192.168.1.79/MindfulMinutesService/AppResource/MyUploadResource/Audio/",
        ResourcePdfUrl = "http://192.168.1.79/MindfulMinutesService/AppResource/MyUploadResource/Pdf/";

// Notify Plugin Start
//-----------------------------------------------
function showAlert(message, alert_type) {
    $.notify({
        // options
        message: message
    }, {
        // settings
        type: alert_type,
        delay: 2000,
        offset: {
            y: 100,
            x: 20
        },
        placement: {
            from: "bottom",
            align: "center"
        },
        timer: 1000
    });
}
//-----------------------------------------------
// Notify Plugin End

var ReminderTextArr = [
    "Remember your breath, pay attention to the movement of your body.",
    "Come back to the most dominant sensation in your body, and feel it with your full attention.",
    "May i be safe, may i be well, may i be happy.",
    ""
];

function getPhoneGapiOSPath(options) {
    'use strict';
    var path = window.location.pathname;
    var phoneGapPath = path.substring(0, path.lastIndexOf('/') + 1);
    return (phoneGapPath + options);
};

var deviceInfo = window.localStorage.getItem("deviceInfo") != null ? JSON.parse(window.localStorage.getItem("deviceInfo")) : { token: "", platform: "" };

var userData = window.localStorage.getItem("userData") != null ? JSON.parse(window.localStorage.getItem("userData")) : null;

var _deviceLocations = { lat: "", lng: "" };

function GetUsersCurrentLocation(locationCallback) {
    navigator.geolocation.getCurrentPosition(
        function (position) {
            locationCallback(position);
        },
        function (error) {
            //alert('code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
            showAlert(('code: ' + error.code + '\n' + 'message: ' + error.message + '\n'), "danger");
        },
        { enableHighAccuracy: true }
        );
}

// Daily Reminder (Local Notification) Start
document.addEventListener('deviceready', function () {

    //Determine permission to show local notifications
    cordova.plugins.notification.local.hasPermission(function (granted) {
        // console.log('Permission has been granted: ' + granted);
    });

    //Register permission to show local notifications
    cordova.plugins.notification.local.registerPermission(function (granted) {
        // console.log('Permission has been granted: ' + granted);
    });

    if (userData.UserSettingsModel.IsReminderActive && userData.UserSettingsModel.ReminderTime != "") {

        var now = new Date();
        var reminder_time_now = new Date(userData.UserSettingsModel.ReminderDateTime);
        reminder_time_now.setDate(now.getDate());
        reminder_time_now.setMonth(now.getMonth());
        reminder_time_now.setFullYear(now.getFullYear());

        console.log("Schedule Time : " + reminder_time_now);

        //Schedule notifications        
        cordova.plugins.notification.local.schedule({
            id: 1,
            title: 'Mindful Minutes',
            text: userData.UserSettingsModel.ReminderMessage,
            //icon: "http://3.bp.blogspot.com/-Qdsy-GpempY/UU_BN9LTqSI/AAAAAAAAAMA/LkwLW2yNBJ4/s1600/supersu.png",
            every: 'day',
            at: reminder_time_now
        });

        cordova.plugins.notification.local.on('trigger', function (notification) {
            console.log('ontrigger : ' + JSON.stringify(notification));
        });

        cordova.plugins.notification.local.on('schedule', function (notification) {
            console.log('onschedule : ' + JSON.stringify(notification));
        });
    }

}, false);
//Daily Reminder (Local Notification) End

// Sign Up
function SignUp() {

    Data = {
        "model": {
            Name: $('#FullName').val(),
            Location: $('#Location').val(),
            Email: $('#Email').val(),
            Password: $("#Password").val(),
            RoleId: roleId,
            ProfileImage: "",
            DeviceToken: deviceInfo.token,
            DeviceType: deviceInfo.platform
        }
    };

    callname = "SignUp";
    callMindfulService(JSON.stringify(Data), callname);
}
function SignUpCallback(result) {
    window.localStorage.setItem("userData", result.Response.Data);

    userData = JSON.parse(result.Response.Data);

    userData.UserSettingsModel.BackgroundImage = (TimerBgPath + userData.UserSettingsModel.BackgroundImage + ".jpg");
    userData.UserSettingsModel.BellStrikeInterval = userData.UserSettingsModel.BellStrikeInterval.split(' ')[0].trim();

    window.localStorage.setItem("userData", JSON.stringify(userData));

    location.href = redirectAfterLogin;
}

//Sign In
function SignIn() {

    deviceInfo = window.localStorage.getItem("deviceInfo") != null ? JSON.parse(window.localStorage.getItem("deviceInfo")) : { token: "", platform: "" };
    //_deviceLocations = JSON.parse(window.sessionStorage.getItem("deviceLocations"));

    Data = {
        "model": {
            Username: $('#Username').val(),
            Password: $("#Password").val(),
            //Latitude: _deviceLocations.lat,
            //Longitude: _deviceLocations.lng,
            Latitude: "",
            Longitude: "",
            DeviceToken: deviceInfo.token,
            DeviceType: deviceInfo.platform
        }
    };

    callname = "SignIn";
    callMindfulService(JSON.stringify(Data), callname);
}

function SignInCallback(result) {
    window.localStorage.setItem("userData", result.Response.Data);

    userData = JSON.parse(result.Response.Data);

    if (userData.UserSettingsModel.OpeningScreen != "") {
        redirectAfterLogin = userData.UserSettingsModel.OpeningScreen + screenextension;
    }

    userData.UserSettingsModel.BackgroundImage = (TimerBgPath + userData.UserSettingsModel.BackgroundImage + ".jpg");
    userData.UserSettingsModel.BellStrikeInterval = userData.UserSettingsModel.BellStrikeInterval.split(' ')[0].trim();

    window.localStorage.setItem("userData", JSON.stringify(userData));

    location.href = redirectAfterLogin;
}

//Sign Out
function SignOut() {
    window.localStorage.clear();
    window.sessionStorage.clear();
    window.location.href = "index.html";
}

// Forgot Password
function RequestPassword() {
    Data = {
        "model": {
            Name: $('#Name').val(),
            Email: $('#Email').val(),
        }
    };

    callname = "ForgotPassword";
    callMindfulService(JSON.stringify(Data), callname);
}

function RequestPasswordCallback(result) {
    //alert(result.Response.Message);
    showAlert(result.Response.Message, "info");
}

//Get Profile
function GetProfile() {
    Data = {
        "Id": userData.Id
    };

    callname = "GetMemberProfile";
    callMindfulService(JSON.stringify(Data), callname);
}

function GetProfileCallback(result) {

    var profile = JSON.parse(result.Response.Data);

    if (!isProfileEdit) {
        $('#profilePic').attr('src', profile.ProfileImage);
        $('#profileName').text(profile.Name);
        $('#profileAddress').text(profile.Location);

        //var timerHours = profile.MemberStats.TimerHours != "" ? profile.MemberStats.TimerHours.split(':') : "0";
        //console.log(timerHours);
        //if((timerHours[0]=="0" || timerHours[0]=="00"))

        var totalHours = 0;
        if (profile.MemberStats.TimerHours > 0) {
            $('#txtTimerHours').addClass("heading-digit-2");

            totalHours = parseFloat(profile.MemberStats.TimerHours);
        }

        if (profile.MemberStats.GuidedHours > 0) {
            $('#txtGuidedHours').addClass("heading-digit-2");

            totalHours = (totalHours + parseFloat(profile.MemberStats.GuidedHours)).toFixed(2);;
        }

        if (profile.MemberStats.GuidedHours > 0 || profile.MemberStats.TimerHours > 0) {
            $('#txtTotalHours').addClass("heading-digit-2");
        }

        $('#txtTimerHours').text(profile.MemberStats.TimerHours);
        $('.txtSinceRegister').text("Since " + profile.DateSinceRegister);
        $('#txtGuidedHours').text(profile.MemberStats.GuidedHours);
        $('#txtTotalHours').text(totalHours);

        var recentGroupHtml = "";
        if (profile.RecentlyJoinedGroup != null && profile.RecentlyJoinedGroup.length > 0) {

            for (var i = 0; i < profile.RecentlyJoinedGroup.length; i++) {

                var recentJoinedGroup = profile.RecentlyJoinedGroup[i];

                recentGroupHtml += '<div class="row mtb15">\
                                <div class="col-xs-3"><img src="' + recentJoinedGroup.GroupImage + '" class="img-thumbnail" width="100%"></div>\
                                <div class="col-xs-9 mtb10" style="padding-left:0;">\
                                    <div>Joined <b class="text-white">' + recentJoinedGroup.GroupName + '</b></div>\
                                    <div class="text-muted"><small>about ' + recentJoinedGroup.JoiningDate + '</small></div>\
                                </div>\
                            </div>';
            }
        } else {
            recentGroupHtml = '<div class="row mtb15">\
                                <div class="col-xs-12 mtb10" style="padding-left:0;">\
                                    <p class="text-center">No recent group found.</p>\
                                </div>\
                            </div>';
        }

        $("#recentGroup").html(recentGroupHtml);

        var joinedGroup = "";
        if (profile.JoinedGroup != null && profile.JoinedGroup.length > 0) {

            for (var i = 0; i < profile.JoinedGroup.length; i++) {

                var joinedGroupModel = profile.JoinedGroup[i];

                joinedGroup = '<div class="row mtb15">\
                                <a href="javascript:void(0);">\
                                    <div class="col-xs-4"><img src="' + joinedGroupModel.GroupImage + '" width="100%" class="img-thumbnail"></div>\
                                    <div class="col-xs-8" style="padding-left:0;">\
                                        <div><b class="text-white">' + joinedGroupModel.GroupName + '</b></div>\
                                        <div class="text-muted">' + joinedGroupModel.GroupDescription + '</div>\
                                        <div class="mtb10 text-gray"><i class="fa fa-group"></i> ' + joinedGroupModel.TotalMembers + '</div>\
                                    </div>\
                                </a>\
                            </div>';
            }
        } else {
            joinedGroup = '<div class="row mtb15">\
                                <div class="col-xs-12 mtb10" style="padding-left:0;">\
                                    <p class="text-center">No group found.</p>\
                                </div>\
                            </div>';
        }

        $("#joinedGroup").html(joinedGroup);

    } else {
        $('#profilePic').attr('src', profile.ProfileImage);
        $('#profileName').val(profile.Name);
        $('#profileAddress').val(profile.Location);
        $('#profileTagline').val(profile.TagLine);
        $('#profileWebsite').val(profile.Website);
        $('#profileAbout').val(profile.About);
        $('#profilePaypalEmail').val(profile.PaypalEmail)
        $('#cbMilestones').prop('checked', profile.DisplayMilestones);
        isMilestoneChecked = profile.DisplayMilestones;
        $('#cbStats').prop('checked', profile.DisplayStats);
        isStatsChecked = profile.DisplayStats;
        $('#cbFriendRequest').prop('checked', profile.AllowFriendRequest);
        isFriendRequestChecked = profile.AllowFriendRequest;
    }
}

//Edit Profile Picture
function EditPictureCallback(result) {
    var picture = JSON.parse(result.Response.Data);
    $('#profilePic').attr('src', picture.ProfileImage);
}

//Edit Profile
var isMilestoneChecked = false,
    isStatsChecked = false,
    isFriendRequestChecked = false;

function EditProfile() {
    Data = {
        "model": {
            "Id": userData.Id,
            "Name": $('#profileName').val(),
            "Location": $('#profileAddress').val(),
            "TagLine": $('#profileTagline').val(),
            "Website": $('#profileWebsite').val(),
            "About": $('#profileAbout').val(),
            "DisplayMilestones": isMilestoneChecked,
            "DisplayStats": isStatsChecked,
            "AllowFriendRequest": isFriendRequestChecked,
            "PaypalEmail": $('#profilePaypalEmail').val()
        }
    };

    callname = "EditMemberProfile";
    callMindfulService(JSON.stringify(Data), callname);
}

function EditProfileCallback(result) {
    window.localStorage.setItem("userData", result.Response.Data);
    location.href = "profile.html";
}

// User Settings
var newUserSettigs;

function SetNewUserSettings() {
    if (window.sessionStorage.getItem("newUserSettigs") != null) {
        newUserSettigs = JSON.parse(window.sessionStorage.getItem("newUserSettigs"));
    } else {
        newUserSettigs = {
            Id: userSettings.Id,
            MemberId: userData.Id,
            BellStrikeInterval: userSettings.BellStrikeInterval,
            BackgroundImage: userSettings.BackgroundImage,
            IsReminderActive: userSettings.IsReminderActive,
            ReminderTime: userSettings.ReminderTime,
            ReminderDateTime: userSettings.ReminderDateTime,
            ReminderMessage: userSettings.ReminderMessage,
            OpeningScreen: userSettings.OpeningScreen,
            ShowNotification: userSettings.ShowNotification,
            JustMeditatedWith: userSettings.JustMeditatedWith
        }
        window.sessionStorage.setItem("newUserSettigs", JSON.stringify(newUserSettigs));
    }
}

$('#btnSetScreen').click(function () {
    $('input[name=filter]').each(function () {
        if ($(this).is(':checked')) {
            $('#OpeningScreen').text($(this).data("titletext"));
            newUserSettigs.OpeningScreen = this.value;
            window.sessionStorage.setItem("newUserSettigs", JSON.stringify(newUserSettigs));
            $('#openingscreen').modal('hide');
        }
    });
});

$('#btnSetBell').click(function () {
    $('input[name=filter]').each(function () {
        if ($(this).is(':checked')) {
            $('#bellInterval').text(this.value);
            newUserSettigs.BellStrikeInterval = this.value;
            window.sessionStorage.setItem("newUserSettigs", JSON.stringify(newUserSettigs));
            $('#bell').modal('hide');
        }
    });
});

$('#btnSetImage').click(function () {
    $('input[name=backgroundimage]').each(function () {
        if ($(this).is(':checked')) {
            $('#backgroundImage').text(this.value);
            newUserSettigs.BackgroundImage = this.value;
            window.sessionStorage.setItem("newUserSettigs", JSON.stringify(newUserSettigs));
            $('#backgroundImageModal').modal('hide');
        }
    });
});

// Save User Settings
function SaveUserSettings() {
    Data = "{\"model\":" + window.sessionStorage.getItem("newUserSettigs") + "}";

    callname = "EditUserSettings";
    callMindfulService(Data, callname);
}

function SaveUserSettingsCallback(result) {
    //alert(result.Response.Message);
    showAlert(result.Response.Message, "success");
    GetUserSettingsCallback(result);
}

// Get User Settings
function GetUserSettings() {
    Data = {
        "Id": userData.Id
    };

    callname = "GetUserSettings";
    callMindfulService(JSON.stringify(Data), callname);
}

var ShowNotification = false,
    hdnOpeningScreen = "dashboard",
    IsReminderActive = false,
    JustMeditatedWith = false;

var userSettings = null;

function GetUserSettingsCallback(result) {

    userSettings = JSON.parse(result.Response.Data);
    window.sessionStorage.setItem("userSettings", result.Response.Data);

    hdnOpeningScreen = userSettings.OpeningScreen != "" ? userSettings.OpeningScreen : "dashboard";

    $('input[name=filter]').each(function () {
        if (this.value == hdnOpeningScreen) {
            $(this).prop('checked', true);
            $('#OpeningScreen').text($(this).data("titletext"));
        }
    });

    ShowNotification = userSettings.ShowNotification;
    $('#cbShowNotification').prop('checked', ShowNotification);

    userData.UserSettingsModel.BackgroundImage = (TimerBgPath + userSettings.BackgroundImage + ".jpg");
    userData.UserSettingsModel.BellStrikeInterval = userSettings.BellStrikeInterval.split(' ')[0].trim();
    userData.UserSettingsModel.IsReminderActive = userSettings.IsReminderActive;
    userData.UserSettingsModel.ReminderTime = userSettings.ReminderTime;
    userData.UserSettingsModel.ReminderDateTime = userSettings.ReminderDateTime;
    userData.UserSettingsModel.ReminderMessage = userSettings.ReminderMessage;
    userData.UserSettingsModel.ShowNotification = userSettings.ShowNotification;
    userData.UserSettingsModel.JustMeditatedWith = userSettings.JustMeditatedWith;

    window.localStorage.setItem("userData", JSON.stringify(userData));

    SetNewUserSettings();
}

// Get Timer Settings
function GetTimerSettings() {
    userSettings = JSON.parse(window.sessionStorage.getItem("userSettings"));

    $('input[name=backgroundimage]').each(function () {
        if (this.value == userSettings.BackgroundImage) {
            $(this).prop('checked', true);
            $('#backgroundImage').text(this.value);
        }
    });

    $('input[name=filter]').each(function () {
        if (this.value == userSettings.BellStrikeInterval) {
            $(this).prop('checked', true);
            $('#bellInterval').text(userSettings.BellStrikeInterval);
        }
    });

    SetNewUserSettings();
}

// Get Daily Reminder Settings
function GetDailyReminderSettings() {
    userSettings = JSON.parse(window.sessionStorage.getItem("userSettings"));

    IsReminderActive = userSettings.IsReminderActive;
    $('#cbDailyReminder').prop('checked', IsReminderActive);

    $('#MeditationReminder').text(userSettings.ReminderMessage);

    $('#txtMeditationReminder').val(userSettings.ReminderMessage);

    //console.log(new Date().toLocaleTimeString().replace(/:\d{2}\s/, ' '));

    var now = new Date(),
        newDate = new Date(userSettings.ReminderDateTime);
    newDate.setDate(now.getDate());
    newDate.setMonth(now.getMonth());
    newDate.setFullYear(now.getFullYear());

    var times = $('#datetimepicker').datetimepicker({
        inline: true,
        sideBySide: true,
        format: 'LT',
        defaultDate: moment(newDate)
    }).on("dp.change", function (e) {
        var date = e.date;//e.date is a moment object
        var target = $(e.target).attr('name');
        console.log(date.format("hh:mmA"))//get time by using format
        console.log(date.format("YYYY-MM-DD hh:mmA"));

        newUserSettigs.ReminderTime = date.format("hh:mmA");
        newUserSettigs.ReminderDateTime = date.format("YYYY-MM-DD hh:mmA");

        window.sessionStorage.setItem("newUserSettigs", JSON.stringify(newUserSettigs));
    });

    SetNewUserSettings();
}

// Save Reminder Message
function SaveReminderMessage() {
    $('#MeditationReminder').text($('#txtMeditationReminder').val());
    newUserSettigs.ReminderMessage = $('#txtMeditationReminder').val();
    window.sessionStorage.setItem("newUserSettigs", JSON.stringify(newUserSettigs));
    $('#ReminderMessgeModal').modal('hide');
}

// Get Privacy Settings
function GetPrivacySettings() {
    userSettings = JSON.parse(window.sessionStorage.getItem("userSettings"));

    JustMeditatedWith = userSettings.JustMeditatedWith;
    $('#cbJustMeditatedWith').prop('checked', JustMeditatedWith);

    SetNewUserSettings();
}

// Timer Screen
//Get Ambient Sound
function GetAmbientSound() {
    Data = {
        "UserID": userData.Id
    };
    callname = "GetAmbientSoundList";
    callMindfulService(JSON.stringify(Data), callname);
}

function GetAmbientSoundCallback(result) {
    var soundList = JSON.parse(result.Response.Data);

    if (soundList.length > 0) {
        var sound_html = "";

        for (var i = 0; i < soundList.length; i++) {

            if (i % 3 == 0) {
                sound_html += '<div class="row btn-group-justified" role="group" aria-label="..." style="margin:0;">';
            }
            sound_html += '<div class="col-xs-4 col-sm-4 pad5 sounds" data-soundname="' + soundList[i].SoundName + '" data-soundid="' + soundList[i].SoundId + '" data-soundfilename="' + soundList[i].SoundFileName + '">\
                            <img src="' + soundList[i].SoundImage + '" class="img-thumbnail" width="100%">';

            if (soundList[i].IsPaid) {
                sound_html += '<a class="locked-sound" data-toggle="modal" onclick="UpgradeSound(' + soundList[i].SoundId + ',\'' + soundList[i].SoundName.trim() + '\')"><div class="padlock"><i class="fa fa-lock fa-3x"></i></div></a>';
                //sound_html += '<a data-toggle="modal" data-target="#soundupgread"><div class="padlock"><i class="fa fa-lock fa-3x"></i></div></a>';
            }

            if (i == 0) {
                sound_html += '<div class="ambient_sound_check"><i class="fa fa-3x text-white fa-check"></i></div>';
            }

            sound_html += '</div>';

            if (i % 3 == 2) {
                sound_html += '</div>';
            }

            if (i == 0) {
                $('#btnAmbientSound').val(soundList[i].SoundName);
                $('#btnAmbientSound').attr("data-selectedsoundid", soundList[i].SoundId);
                $('#btnAmbientSound').attr('data-selectedsoundfilename', soundList[i].SoundFileName);
            }
        }
        $('#ambientSoundContainer').html(sound_html);
    }

    $('.sounds').click(function () {
        var $this = $(this);
        if (!$this.children('.locked-sound').children('.padlock').length) {
            if ($('.ambient_sound_check').length) {
                if (!$this.children('.ambient_sound_check').length) {
                    $('.ambient_sound_check').remove();
                    $this.append('<div class="ambient_sound_check"><i class="fa fa-3x text-white fa-check"></i></div>');
                } else {
                    $('.ambient_sound_check').remove();
                }
            } else {
                $this.append('<div class="ambient_sound_check"><i class="fa fa-3x text-white fa-check"></i></div>');
            }
        } else {
            $('#soundupgread').modal('show');
        }
    });

    SetTimer();
}

// Play Timer Start
var timerOptions = null;

function SetTimer() {

    if (window.sessionStorage.getItem("timerOptions") != null) {

        timerOptions = JSON.parse(window.sessionStorage.getItem("timerOptions"));

        $('.indicators1 button[type=button]').each(function () {
            if ($(this).data("startsound") == timerOptions.StartingSound) {
                $('.indicators1 button[type=button]').removeClass('active');
                $(this).addClass('active');

                var active_slide_no = $(this).data("slide-to");

                $('#carousel1 .carousel-inner .item').each(function () {
                    if ($(this).data("slide-no") == active_slide_no) {
                        $('#carousel1 .carousel-inner .item').removeClass('active');
                        $(this).addClass('active');
                    }
                });
            }
        });

        $('.indicators2 button[type=button]').each(function () {
            if ($(this).data("endsound") == timerOptions.EndingSound) {
                $('.indicators2 button[type=button]').removeClass('active');
                $(this).addClass('active');

                var active_slide_no = $(this).data("slide-to");

                $('#carousel2 .carousel-inner .item').each(function () {
                    if ($(this).data("slide-no") == active_slide_no) {
                        $('#carousel2 .carousel-inner .item').removeClass('active');
                        $(this).addClass('active');
                    }
                });
            }
        });

        $('#TimerLength').val(timerOptions.TimerLength);
        $('#ddlTimerVol').val(timerOptions.TimerVolume);

        if (timerOptions.IsRepeat) {
            $('#DivReminderInstruction').hide();
        } else {
            $('#DivReminderInstruction').show();
        }

        $('#btnAmbientSound').val(timerOptions.AmbientSoundName);

        $('#ReminderInstruction').val(timerOptions.ReminderFor);

        if (timerOptions.NoOfReminder > 0) {
            $('#NoOfReminder').val(timerOptions.NoOfReminder);
        }

        $('#reminder [data-toggle="buttons-radio"] button').each(function () {

            if ($(this).text() == timerOptions.ReminderFor) {

                $('#reminder [data-toggle="buttons-radio"] button').removeClass('active');

                $(this).addClass('active');

                var $this = ($(this).attr('id') + '_text');

                $('#ReminderInstruction').val(timerOptions.ReminderFor);
                $('#' + $this).show();

                timerOptions.ReminderInstruction = $('#' + $this).children('p').text();
                timerOptions.NoOfReminder = $('#NoOfReminder').val();
                window.sessionStorage.setItem("timerOptions", JSON.stringify(timerOptions));
            }
        });

        $('#TimerDelay').val(timerOptions.TimerDelay + 's');
        $('#ddlDelay').val(timerOptions.TimerDelay);

        if (timerOptions.Id > 0) {
            $('.timer_preset').html("Save<br>Changes");
            $('#PresetTitle').val(timerOptions.PresetTitle);
        }

        window.sessionStorage.setItem("timerOptions", JSON.stringify(timerOptions));

    } else {

        var mDate = "2016-01-01 00:10:00";

        timerOptions = {
            Id: 0,
            StartingSound: $('.indicators1 button[type=button].active').data('startsound'),
            EndingSound: $('.indicators2 button[type=button].active').data('endsound'),
            IsRepeat: false,
            TimerLength: $('#TimerLength').val(),
            TimerLengthDateTime: mDate,
            AmbientSoundId: $('#btnAmbientSound').data('selectedsoundid'),
            AmbientSoundName: $('#btnAmbientSound').val(),
            AmbientSoundFileName: $('#btnAmbientSound').data('selectedsoundfilename'),
            ReminderFor: 'None',
            ReminderInstruction: "",
            NoOfReminder: 0,
            TimerDelay: $('#TimerDelay').data('delaytime'),
            TimerVolume: $('#ddlTimerVol').val(),
            PresetTitle: $('#PresetTitle').val(),
            MemberId: userData.Id
        };
        window.sessionStorage.setItem("timerOptions", JSON.stringify(timerOptions));
    }
}

$('#btnAmbientSound').click(function () {

    if (timerOptions.Id > 0) {
        var divSounds = $('#ambientsound #ambientSoundContainer .row .sounds');

        divSounds.each(function () {
            if ($(this).attr("data-soundid") == timerOptions.AmbientSoundId) {
                $('.ambient_sound_check').remove();
                $(this).append('<div class="ambient_sound_check"><i class="fa fa-3x text-white fa-check"></i></div>');
            }
        });
    }
});

function StartTimer() {
    timerOptions = JSON.parse(window.sessionStorage.getItem("timerOptions"));

    //timerOptions.StartingSound = $('.indicators1 button[type=button].active').data('startsound');
    //timerOptions.EndingSound = $('.indicators2 button[type=button].active').data('endsound');
    //timerOptions.AmbientSoundId = $('#btnAmbientSound').data('selectedsoundid');
    //timerOptions.AmbientSoundName = $('#btnAmbientSound').val();
    //timerOptions.AmbientSoundFileName = $('#btnAmbientSound').data('selectedsoundfilename');
    //timerOptions.ReminderFor = $('#ReminderInstruction').val();
    //timerOptions.ReminderInstruction = $('#ReminderInstruction').data("data-reminder");
    //timerOptions.NoOfReminder = $('#ReminderInstruction').data("noofreminder");
    //timerOptions.TimerDelay = $('#TimerDelay').data('delaytime');
    //timerOptions.TimerVolume = $('#ddlTimerVol').val();

    //window.sessionStorage.setItem("timerOptions", JSON.stringify(timerOptions));

    window.location = "timer_play.html";
}
// Play Timer End

// Save Preset Timer Start
function SavePresetTimer() {

    var presetTitle = $('#PresetTitle').val();
    if (presetTitle != "" && presetTitle != null) {
        $('#PresetTitle').removeAttr('style');

        timerOptions = JSON.parse(window.sessionStorage.getItem("timerOptions"));

        timerOptions.PresetTitle = $('#PresetTitle').val();
        timerOptions.MemberId = userData.Id

        //Ignore this statement
        delete timerOptions.AmbientSoundName;
        delete timerOptions.AmbientSoundFileName;
        delete timerOptions.ReminderInstruction;

        Data = "{\"model\":" + JSON.stringify(timerOptions) + "}";

        callname = "AddPresetTimer";
        callMindfulService(Data, callname);
    } else {
        $('#PresetTitle').css("border", "1px solid red");
        $('#PresetTitle').focus();
    }
}

function SavePresetTimerCallback(result) {
    $('#modal_savepreset').modal('hide');
    window.sessionStorage.removeItem("timerOptions");
    window.location = "preset.html";
}

$('#btnCancelPreset').click(function () {
    $('#PresetTitle').removeAttr('style');
    if (timerOptions.Id > 0) {
        $('.timer_preset').html("Save<br>Changes");
        $('#PresetTitle').val(timerOptions.PresetTitle);
    } else {
        $('#PresetTitle').val('');
    }
    $('#modal_savepreset').modal('hide');
});
// Save Preset Timer End

// Get Preset Timer List Start
function GetPresetTimerList() {
    Data = {
        "MemberId": userData.Id
    };

    callname = "GetPresetTimerList";
    callMindfulService(JSON.stringify(Data), callname);
}

function GetPresetTimerListCallback(result) {
    var presetList = JSON.parse(result.Response.Data);
    var presetHtml = "",
        timerLengthStr = "";
    var presetModel = {};

    if (presetList.length > 0) {

        presetHtml += '<label>Choose</label>';

        for (var i = 0; i < presetList.length; i++) {
            presetModel = presetList[i];

            timerLengthStr = "";

            if (presetModel.TimerLength.toLowerCase() == "infinity") {
                timerLengthStr = presetModel.TimerLength;
            }
            else {
                var timerLengthArr = presetModel.TimerLength.split(":");
                timerLengthStr = "";

                if (timerLengthArr[0] != "00") {
                    timerLengthStr += (timerLengthArr[0] < 10 ? timerLengthArr[0].substring(1, 2) : timerLengthArr[0]) + " hours";
                } else if (timerLengthArr[1] != "00") {
                    timerLengthStr += (timerLengthArr[1] < 10 ? timerLengthArr[1].substring(1, 2) : timerLengthArr[1]) + " mins";
                } else if (timerLengthArr[2] != "00") {
                    timerLengthStr += (timerLengthArr[2] < 10 ? timerLengthArr[2].substring(1, 2) : timerLengthArr[2]) + " sec";
                }
            }

            presetHtml += '<div class="form-group">\
                              <a>\
                                <div class="input-group" role="group" aria-label="...">\
                                     <span class="input-group-addon">\
                                        <input type="checkbox" name="cbPreset" id="cbPreset_' + presetModel.Id + '" aria-label="...">\
                                     </span>\
                                     <input type="button" id="btnPreset_' + presetModel.Id + '" class="form-control text-center" value="' + timerLengthStr + ' Timer" \
                                            onclick="GetPresetTimer(' + presetModel.Id + ')">\
                                </div>\
                              </a>\
                              <div class="text-center mtb5" id="">\
                                <small>Duration has been set for ' + timerLengthStr + ' with ' + presetModel.TimerDelay + ' sec delays.</small>\
                              </div>\
                           </div>';
        }

        presetHtml += '<div class="form-group">\
                        <a id="btnDeletePreset" class="btn btn-block btn-lg btn-danger text-uppercase">delete</a>\
                        </div>';

        $('#DivPresetList').html(presetHtml);

        $('#DivPresetList input[name=cbPreset]').click(function () {

            var idarr = $(this).attr('id').split('_');

            if ($(this).is(':checked')) {
                deleteIds.push(idarr[1]);
            } else {
                deleteIds.splice(deleteIds.indexOf(idarr[1]), 1);
            }
            console.log(JSON.stringify(deleteIds));
        });

        $('#btnDeletePreset').click(function () {

            strDeleteIds = deleteIds.length > 0 ? deleteIds.join(",") : "";
            console.log(strDeleteIds);
            if (strDeleteIds != "") {

                if (confirm("Are You Sure !") == true) {

                    Data = {
                        Ids: strDeleteIds,
                        MemberId: userData.Id
                    };

                    callname = "DeletePresetTimer";
                    callMindfulService(JSON.stringify(Data), callname);
                }
            } else {
                //alert("Please Select Preset To Delete");
                showAlert("Please Select Preset To Delete", "warning");
            }
        });

    } else {
        $('#DivPresetList').html("<h5 class=\"text-center\">No Preset Saved !");
    }
}

var deleteIds = [];
var strDeleteIds = "";


function DeletePresetTimerCallback(result) {
    strDeleteIds = "";

    location.reload();
}

function GetPresetTimer(Id) {
    Data = {
        Id: Id
    };

    callname = "GetPresetTimerByPresetId";
    callMindfulService(JSON.stringify(Data), callname);
}

function GetPresetTimerCallback(result) {
    window.sessionStorage.removeItem("timerOptions");

    var preset = JSON.parse(result.Response.Data);
    var rfor = preset.ReminderFor;

    if (rfor == "Breath") rfor = 0;
    else if (rfor == "Body") rfor = 1;
    else if (rfor == "Loving Kindness") rfor = 2;
    else if (rfor == "None") rfor = 3;


    timerOptions = {
        Id: preset.Id,
        StartingSound: preset.StartingSound,
        EndingSound: preset.EndingSound,
        IsRepeat: preset.IsRepeat,
        TimerLength: preset.TimerLength,
        TimerLengthDateTime: preset.TimerLengthDateTime,
        AmbientSoundId: preset.AmbientSoundModel.SoundId,
        AmbientSoundName: preset.AmbientSoundModel.SoundName,
        AmbientSoundFileName: preset.AmbientSoundModel.SoundFileName,
        ReminderFor: preset.ReminderFor,
        ReminderInstruction: ReminderTextArr[rfor],
        NoOfReminder: preset.NoOfReminder,
        TimerDelay: preset.TimerDelay,
        TimerVolume: preset.TimerVolume,
        PresetTitle: preset.PresetTitle,
        MemberId: userData.Id
    };

    window.sessionStorage.setItem("timerOptions", JSON.stringify(timerOptions));

    window.location = "timer.html";
}
//Get Preset Timer List  End

//Get Location Start
function GetLocation() {

    _deviceLocations = JSON.parse(window.sessionStorage.getItem("deviceLocations"));

    if (_deviceLocations != null) {
        userData.Latitude = _deviceLocations.lat;
        userData.Longitude = _deviceLocations.lng;
    } else {
        userData.Latitude = "35.266505";
        userData.Longitude = "-80.951879";
    }

    window.localStorage.setItem("userData", JSON.stringify(userData));

    Data = {
        Id: userData.Id,
        Latitude: userData.Latitude,
        Longitude: userData.Longitude
    };

    callname = "GetAllUsersLocation";
    callMindfulService(JSON.stringify(Data), callname);
}

function GetAllUsersLocationCallback(result) {
    var locationList = JSON.parse(result.Response.Data);

    if (locationList.length > 0) {

        var latlngObject = {},
            latlngJson = [];

        for (var i = 0; i < locationList.length; i++) {

            if (locationList[i].Latitude != "" && locationList[i].Longitude != "") {
                latlngObject = {
                    lat: locationList[i].Latitude,
                    lng: locationList[i].Longitude
                };

                latlngJson.push(latlngObject);
            }
        }
        $('#txtTotalMeditatingNow').text(latlngJson.length);
        var map = new GoogleMap(latlngJson);
        map.initialize();
    }
}
//Get Location End

//Neurominutes Free Sampler Start
function GetFreeSamplerList() {
    Data = {};
    callname = "GetNeurominutesFreeSamplerList";
    callMindfulService(Data, callname);
}

function GetFreeSamplerCallback(result) {
    var FreeSamplerList = JSON.parse(result.Response.Data);

    if (FreeSamplerList.length > 0) {

        var FreeSampler_html = "",
            icon = "fa-play-circle",
            SoundFilName = "",
            FileType = "";

        FreeSampler_html += '<div class="row"><div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 ">';

        for (var i = 0; i < FreeSamplerList.length; i++) {

            SoundFilName = FreeSamplerList[i].NeuroMinFreeSamplerFile;

            if (SoundFilName != "") {
                if (SoundFilName.lastIndexOf('.') > -1) {
                    var SoundFilNameArr = SoundFilName.split('.');

                    if (SoundFilNameArr[SoundFilNameArr.length - 1] == "pdf") {

                        icon = "fa-file-text-o";
                        FileType = "pdf";

                    } else if (SoundFilName.toLowerCase().indexOf("vimeo.com") >= 0) {

                        icon = "fa-file-video-o";
                        FileType = "video";
                    }
                    else {
                        icon = "fa-play-circle";
                        FileType = "audio";
                    }
                }
            } else {
                icon = "fa-play-circle";
            }

            FreeSampler_html += ' <div class="row" style="border-bottom: 1px solid #222;">';
            FreeSampler_html += ' <a href="" data-toggle="modal" onclick=ShowPopup(' + FreeSamplerList[i].NeuroMinFreeSamplerId + ',"' + SoundFilName + '","' + FileType + '")>';
            FreeSampler_html += ' <div class="col-xs-3 text-center ptb15"><i class="fa fa-5x ' + icon + '")></i></div>';
            FreeSampler_html += ' <div class="col-xs-9 plr0 ptb15">';
            FreeSampler_html += ' <input type="hidden" id="' + FreeSamplerList[i].NeuroMinFreeSamplerId + '" value="' + FreeSamplerList[i].NeuroMinFreeSamplerId + '">';
            FreeSampler_html += ' <div id="dvFreeSamplerFileName" class="mt10" ><b class="text-white"> ' + FreeSamplerList[i].NeuroMinFreeSamplerFileName + '</b></div>';
            FreeSampler_html += ' <div id="dvViewCount" class="mt5 text-muted"><i class="fa fa-group"></i> ' + FreeSamplerList[i].ViewCount + ' views</div>';
            FreeSampler_html += ' </div> </a> </div>';

        }

        FreeSampler_html += ' </div> </div>';

        $('#freeSamplerContainer').html(FreeSampler_html);
    }
}

function IncreaseViewCount(freeSamplerId) {
    Data = {
        NeuroMinFreeSamplerId: freeSamplerId//$("hdnFreeSamplerId").val()        
    };
    callname = "IncreaseNeurominutesFreeSamplerViewCount";
    callMindfulService(JSON.stringify(Data), callname);
}

//function IncreaseViewCountCallback(result) {

//    var FreeSamplerList = JSON.parse(result.Response.Data);

//    if (FreeSamplerList.length > 0) {
//        var FreeSampler_html = "";
//        FreeSampler_html += '<div class="row"><div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 ">';
//        for (var i = 0; i < FreeSamplerList.length; i++) {
//            FreeSampler_html += ' <div class="row" style="border-bottom: 1px solid #222;">';
//            FreeSampler_html += ' <a href="" data-toggle="modal" data-target="#disclaimer">';
//            FreeSampler_html += ' <div class="col-xs-3 text-center ptb15"><i class="fa fa-5x fa-play-circle"></i></div>';
//            FreeSampler_html += ' <div class="col-xs-9 plr0 ptb15">';
//            FreeSampler_html += ' <input type="hidden" id="hdnFreeSamplerId" value="' + FreeSamplerList[i].NeuroMinFreeSamplerId + '">';
//            //FreeSampler_html += ' <div id="dvFreeSamplerFileName" class="mt10" onclick = "IncreaseViewCounts()"><b class="text-white"> ' + FreeSamplerList[i].NeuroMinFreeSamplerFileName + '</b></div>';
//            FreeSampler_html += ' <div id="dvFreeSamplerFileName" class="mt10"><b class="text-white"> ' + FreeSamplerList[i].NeuroMinFreeSamplerFileName + '</b></div>';
//            FreeSampler_html += ' <div id="dvViewCount" class="mt5 text-muted"><i class="fa fa-group"></i> ' + FreeSamplerList[i].ViewCount + '</div>';
//            FreeSampler_html += ' </div> </a> </div>';
//        }
//        FreeSampler_html += ' </div> </div>';
//        $('#freeSamplerContainer').html(FreeSampler_html);
//    }
//}
function IncreaseViewCountCallback(result) {

}

//Neurominutes Free Sampler End

//Neurominutes Tracks start

function GetNeurominTrackList() {
    Data = {
        "UserID": userData.Id
    };
    callname = "GetNeurominutesTrackList";
    callMindfulService(JSON.stringify(Data), callname);
}

function GetNeurominTrackCallback(result) {
    var NeurominTrackList = JSON.parse(result.Response.Data);

    if (NeurominTrackList.length > 0) {
        var NeurominTrack_html = "";
        NeurominTrack_html += '<div class="row">';
        NeurominTrack_html += '<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 ">	';
        for (var i = 0; i < NeurominTrackList.length; i++) {
            NeurominTrack_html += '<div class="row" style="border-bottom:1px solid #222;">';
            if (NeurominTrackList[i].IsPaid)
                NeurominTrack_html += '<a onclick="OpenTrackDetail(' + NeurominTrackList[i].NeuroMinTrackId + ')">';
            else
                NeurominTrack_html += '<a data-toggle="modal" onclick="UpgradeProduct(' + NeurominTrackList[i].NeuroMinTrackId + ',\'' + NeurominTrackList[i].NeuroMinTrackName.trim() + '\')">';
            NeurominTrack_html += '<div class="col-xs-12 mtb10">';
            NeurominTrack_html += '<input type="hidden" id = "hdnNeuroMinTrackId" value = "' + NeurominTrackList[i].NeuroMinTrackId + '" />';
            NeurominTrack_html += '<h5 class="text-white pull-left mt0 mb0">' + NeurominTrackList[i].NeuroMinTrackName + ' </h5>';
            NeurominTrack_html += '<div class="pull-right">';
            NeurominTrack_html += '<i class="fa fa-angle-right text-success fa-2x"></i>';
            NeurominTrack_html += '</div> </div> </a> </div>';
        }
        NeurominTrack_html += ' </div> </div>';
        $('#dvNeurominTracksContainer').html(NeurominTrack_html);
    }
}
//Neurominutes Tracks end


//Neurominutes Tracks Detail start

function GetNeurominTrackDetailList(TrackId) {
    Data = {
        NeuroMinTrackId: TrackId,
        UserId: userData.Id
    };
    callname = "GetNeurominutesTrackDetailListByTrackId";
    callMindfulService(JSON.stringify(Data), callname);
}

function GetNeurominTrackDetailCallback(result) {

    var NeurominTrackDetailList = JSON.parse(result.Response.Data);

    if (NeurominTrackDetailList.length > 0) {

        var NeurominTrackDetail_html = "",
            icon = "fa-play-circle",
            SoundFilName = "",
            FileType = "";

        NeurominTrackDetail_html += '<script src="js/star-rating.min.js"></script>';
        NeurominTrackDetail_html += '<div class="row">';
        NeurominTrackDetail_html += '<div class="col-xs-12 col-sm-12 col-md-12 text-left">';

        NeurominTrackDetail_html += '<div class="row" id="dvNeuroMinTracks">';
        NeurominTrackDetail_html += '<div class="col-xs-12 text-center"><img src="' + 'http://differenzuat.com/mindfulminutesadmin/AppResource/NeurominutesTrack/TrackImage/' + NeurominTrackDetailList[0].NeurominutesTrack.NeuroMinTrackPhoto + '" class="img-thumbnail" width="100px"></div>';
        NeurominTrackDetail_html += '<div class="col-xs-12 text-center">';
        NeurominTrackDetail_html += '<div class="mtb10"><b class="text-white">' + NeurominTrackDetailList[0].NeurominutesTrack.NeuroMinTrackTitle + '</b></div>';
        NeurominTrackDetail_html += '<div class="mtb10 text-gray">';
        NeurominTrackDetail_html += ' <input id="input-4" class="rating rating-loading" data-show-clear="false" data-show-caption="true" data-min="0" data-max="5" data-step="0.1" value="' + NeurominTrackDetailList[0].NeurominutesTrackReview.AvgRate + '">';

        NeurominTrackDetail_html += '</div>';
        NeurominTrackDetail_html += '<div class="mtb10 text-muted">' + NeurominTrackDetailList[0].NeurominutesTrack.VisitCount + ' plays • ' + NeurominTrackDetailList[0].NeurominutesTrackReview.TotalRate + ' ratings</div>';
        NeurominTrackDetail_html += '<div class="text-gray">' + NeurominTrackDetailList[0].NeurominutesTrack.NeuroMinTrackDescription + '</div>';
        NeurominTrackDetail_html += '</div>';
        if (NeurominTrackDetailList[0].NeurominutesTrackLike.IsLike == '1') {

            NeurominTrackDetail_html += '<a class="like" onclick="LikeUnlike(' + NeurominTrackDetailList[0].NeurominutesTrack.NeuroMinTrackId + ')" ><i id="iLikeUnlike" class="fa text-success fa-heart fa-2x"></i></a>';
        }
        else {
            NeurominTrackDetail_html += '<a class="like" onclick="LikeUnlike(' + NeurominTrackDetailList[0].NeurominutesTrack.NeuroMinTrackId + ')"><i id="iLikeUnlike" class="fa fa-heart fa-2x"></i></a>';
        }
        NeurominTrackDetail_html += '</div>';

        NeurominTrackDetail_html += '<div style="border-bottom:1px solid #222;margin:10px -15px;"></div>';

        for (var i = 0; i < NeurominTrackDetailList.length; i++) {

            var d = NeurominTrackDetailList[i].NeuroMinTrackDetailName;

            SoundFilName = NeurominTrackDetailList[i].NeuroMinTrackDetailFile;

            if (SoundFilName != "") {
                if (SoundFilName.lastIndexOf('.') > -1) {
                    var SoundFilNameArr = SoundFilName.split('.');

                    if (SoundFilNameArr[SoundFilNameArr.length - 1] == "pdf") {

                        icon = "fa-file-text-o";
                        FileType = "pdf";

                    } else if (SoundFilName.toLowerCase().indexOf("vimeo.com") >= 0) {

                        icon = "fa-file-video-o";
                        FileType = "video";
                    }
                    else {
                        icon = "fa-play-circle";
                        FileType = "audio";
                    }
                }
            } else {
                icon = "fa-play-circle";
            }

            NeurominTrackDetail_html += '<div class="row">';
            NeurominTrackDetail_html += '<div class="col-xs-5 fa-2x text-center mtb10">' + NeurominTrackDetailList[i].NeuroMinTrackDetailName + '</div>';

            NeurominTrackDetail_html += '<div class="col-xs-2 text-center text-success mtb10">\
                                        <i class="fa fa-3x ' + icon + '" onclick=PlaySound("' + SoundFilName + '",' + NeurominTrackDetailList[i].NeuroMinTrackDetailId + ',"' + FileType + '")></i></div>';
            NeurominTrackDetail_html += '<div class="col-xs-5 fa-2x text-center mtb10" id="dvSoundDuration">' + NeurominTrackDetailList[i].FileDuration + '</div>\
                                            <p id="pFileName"></p>';
            NeurominTrackDetail_html += '</div>';

            NeurominTrackDetail_html += '<div style="border-bottom:1px solid #222;margin:10px -15px;"></div>';
        }

        NeurominTrackDetail_html += ' </div> </div> <div style="margin-bottom:5%;"></div>';
        $('#dvTrackName').html(NeurominTrackDetailList[0].NeurominutesTrack.NeuroMinTrackName);
        $('#dvTrackDetailContainer').html(NeurominTrackDetail_html);
    }
}

function LikeUnlike(TrackId) {
    var userData = JSON.parse(window.localStorage.getItem("userData"));

    Data = {
        NeuroMinTrackId: TrackId,
        UserId: userData.Id
    };
    callname = "LikeUnlikeTrack";
    callMindfulService(JSON.stringify(Data), callname);
}

function GetAllGuidedMeditationCategoryCallback(result) {
    var GuidedMeditationCategoryList = JSON.parse(result.Response.Data);

    if (GuidedMeditationCategoryList.length > 0) {

        var CategoryHtml = '';

        for (var i = 0; i < GuidedMeditationCategoryList.length; i++) {

            var pageURL = '';

            if (GuidedMeditationCategoryList[i].Type == 1) {

                pageURL = 'Basic.html'
            }
            else if (GuidedMeditationCategoryList[i].Type == 2) {
                pageURL = 'Foundations.html'
            }
            else if (GuidedMeditationCategoryList[i].Type == 3) {
                pageURL = 'HeartPractices.html'
            }
            else if (GuidedMeditationCategoryList[i].Type == 4) {
                pageURL = 'Advanced.html'
            }
            else if (GuidedMeditationCategoryList[i].Type == 5) {
                pageURL = 'meditationbook.html'
            }
            else if (GuidedMeditationCategoryList[i].Type == 6) {
                pageURL = 'CommunityCreatedMeditation.html'
            }
            else if (GuidedMeditationCategoryList[i].Type == 7) {
                pageURL = 'myuploadsmeditaion.html'

            }

            CategoryHtml += '<div class="row" style="border-bottom:1px solid #222;">' +
                            '<a href="javascript:void(0);" onclick="GoToGuided(' + GuidedMeditationCategoryList[i].CategoryId + ', \'' + (GuidedMeditationCategoryList[i].CategoryName).trim() + '\'' + ', \'' + pageURL + '\')">' +
                            '<div class="col-xs-12 mtb10">' +
                            '<h5 class="text-white pull-left mt0 mb0">';
            CategoryHtml += GuidedMeditationCategoryList[i].CategoryName;
            CategoryHtml += '</h5><div class="pull-right">' +
                            '<i class="fa fa-angle-right text-success fa-2x"></i>' +
                            '</div>' + '</div>' + '</a>' + '</div>';



        }

        $('#DivGetAllGuidedMeditationCategory').html(CategoryHtml);
    }
}

function GoBackToSubCategory() {
    var Type = window.sessionStorage.getItem("CategoryId"),
        pageURL = "selectcategoty.html";

    if (Type == 1) {
        pageURL = 'Basic.html'
    }
    else if (Type == 2) {
        pageURL = 'Foundations.html'
    }
    else if (Type == 3) {
        pageURL = 'HeartPractices.html'
    }
    else if (Type == 4) {
        pageURL = 'Advanced.html'
    }
    else if (Type == 5) {
        pageURL = 'meditationbook.html'
    }
    else if (Type == 6) {
        pageURL = 'CommunityCreatedMeditation.html'
    }
    else if (Type == 7) {
        pageURL = 'myuploadsmeditaion.html'
    }

    window.location = pageURL;
}

function GoToGuided(CategoryId, CategoryName, pageURL) {
    window.sessionStorage.setItem("CategoryId", CategoryId);
    window.sessionStorage.setItem("CategoryName", CategoryName.trim());
    window.location.href = pageURL;
}
function GetAllGuidedUploadMeditationByCategoryCallback(result) {
    var GuidedUploadMeditationList = JSON.parse(result.Response.Data);
    if (GuidedUploadMeditationList.length > 0) {
        var GuidedUploadMeditationHtml = '';

        GuidedUploadMeditationHtml += '<script src="js/star-rating.min.js"></script>';

        for (var i = 0; i < GuidedUploadMeditationList.length; i++) {

            var likeClass = "text-success";

            if (GuidedUploadMeditationList[i].IsFavourite == false) {
                likeClass = "text-failure"
            }

            if (GuidedUploadMeditationList[i].IsPaid == false) {

                GuidedUploadMeditationHtml +=
                    '<div class="row ptb15">' +
                    '<a onclick="GoToViewGuided(' + GuidedUploadMeditationList[i].GuidedUploadMeditationId + ')">' +
                        '<div class="col-xs-4"><img src="' + GuidedUploadMeditationList[i].GuidedProfileImage + '" width="100%" class="img-thumbnail"></div>' +
                        '<div class="col-xs-8" style="padding-left:0;">' +
                            '<div><b class="text-white">' + GuidedUploadMeditationList[i].Title + '</b></div>' +
                            '<div class="text-muted small">' + GuidedUploadMeditationList[i].NoOfPlays + ' plays | ' + GuidedUploadMeditationList[i].TotalRating + ' ratings</div>' +
                            '<div class="text-muted small">' + GuidedUploadMeditationList[i].GuidedName + '</div>' +
                            ' <div class="starrating" disabled>' +
                                '<input disabled id="input_' + GuidedUploadMeditationList[i].GuidedUploadMeditationId + '" value="' + GuidedUploadMeditationList[i].AverageRating + '"  class="rating rating-loading" data-show-clear="false" data-show-caption="true" data-min="0" data-max="5" data-step="0.1">' +
                            '</div>' +
                        '</div>' +
                    '</a>' +
                    '<a class="like"  id="' + GuidedUploadMeditationList[i].GuidedUploadMeditationId + '" onclick="AddRemoveFavourites(this)"><i class="fa ' + likeClass + ' fa-heart fa-2x"></i></a>' +
                    '</div>';
            }
            else {
                GuidedUploadMeditationHtml +=
                    '<div class="row ptb15">' +
                    '<a data-toggle="modal" onclick="UpgradeProduct(' + GuidedUploadMeditationList[i].GuidedUploadMeditationId + ',\'' + GuidedUploadMeditationList[i].Title.trim() + '\')">' +
                    '<div class="padlock"><i class="fa fa-lock fa-4x"></i></div>' +
                        '<div class="col-xs-4"><img src="' + GuidedUploadMeditationList[i].GuidedProfileImage + '" width="100%" class="img-thumbnail"></div>' +
                        '<div class="col-xs-8" style="padding-left:0;">' +
                            '<div><b class="text-white">' + GuidedUploadMeditationList[i].Title + '</b></div>' +
                            '<div class="text-muted small">' + GuidedUploadMeditationList[i].NoOfPlays + ' plays | 15.0k ratings</div>' +
                            '<div class="text-muted small">' + GuidedUploadMeditationList[i].GuidedName + '</div>' +
                            ' <div class="starrating" disabled>' +
                                '<input  disabled id="input_' + GuidedUploadMeditationList[i].GuidedUploadMeditationId + '" value="' + GuidedUploadMeditationList[i].AverageRating + '" disable class="rating rating-loading" data-show-clear="false" data-show-caption="true" data-min="0" data-max="5" data-step="0.1">' +
                            '</div>' +
                        '</div>' +
                    '</a>' +
                    '<a class="like"   id="' + GuidedUploadMeditationList[i].GuidedUploadMeditationId + '" onclick="AddRemoveFavourites(this)"><i class="fa ' + likeClass + ' fa-heart fa-2x"></i></a>' +
                    '</div>';
            }

        }
    }
    else {
        GuidedUploadMeditationHtml = '<div class="row ptb15"><div class="col-xs-12 text-center">No data found.</div></div>';
    }
    $('#DivGetAllGuidedUploadMeditationByCategory').html(GuidedUploadMeditationHtml);
}

function GetRelatedGuidedUploadMeditationCallback(result) {
    var GuidedUploadMeditationList = JSON.parse(result.Response.Data);
    if (GuidedUploadMeditationList.length > 0) {
        var GuidedUploadMeditationHtml = '';

        GuidedUploadMeditationHtml += '<script src="js/star-rating.min.js"></script>';

        for (var i = 0; i < GuidedUploadMeditationList.length; i++) {

            var likeClass = "text-success";
            if (GuidedUploadMeditationList[i].IsFavourite == false) {
                likeClass = "text-failure"
            }
            if (GuidedUploadMeditationList[i].IsPaid == false) {

                GuidedUploadMeditationHtml +=
                '<div class="row ptb15">' +
                '<a onclick="GoToViewGuided(' + GuidedUploadMeditationList[i].GuidedUploadMeditationId + ')">' +
	                '<div class="col-xs-4"><img src="' + GuidedUploadMeditationList[i].GuidedProfileImage + '" width="100%" class="img-thumbnail"></div>' +
	                '<div class="col-xs-8" style="padding-left:0;">' +
		                '<div><b class="text-primary">' + GuidedUploadMeditationList[i].Title + '</b></div>' +
		                '<div class="text-muted small">' + GuidedUploadMeditationList[i].NoOfPlays + ' plays | ' + GuidedUploadMeditationList[i].TotalRating + ' ratings</div>' +
		                '<div class="text-muted small">' + GuidedUploadMeditationList[i].GuidedName + '</div>' +
		                ' <div class="starrating" disabled>' +
			                '<input disabled id="input_' + GuidedUploadMeditationList[i].GuidedUploadMeditationId + '" value="' + GuidedUploadMeditationList[i].AverageRating + '"  class="rating rating-loading" data-show-clear="false" data-show-caption="true" data-min="0" data-max="5" data-step="0.1">' +
		                '</div>' +
	                '</div>' +
                '</a>' +
                '</div>';
            }
            else {
                GuidedUploadMeditationHtml +=
                    '<div class="row ptb15">' +
                    '<a data-toggle="modal" data-target="#upgrade">' +
                    '<div class="padlock"><i class="fa fa-lock fa-4x"></i></div>' +
                        '<div class="col-xs-4"><img src="' + GuidedUploadMeditationList[i].GuidedProfileImage + '" width="100%" class="img-thumbnail"></div>' +
                        '<div class="col-xs-8" style="padding-left:0;">' +
                            '<div><b class="text-primary">' + GuidedUploadMeditationList[i].Title + '</b></div>' +
                            '<div class="text-muted small">' + GuidedUploadMeditationList[i].NoOfPlays + ' plays | 15.0k ratings</div>' +
                            '<div class="text-muted small">' + GuidedUploadMeditationList[i].GuidedName + '</div>' +
                            ' <div class="starrating" disabled>' +
                                '<input  disabled id="input_' + GuidedUploadMeditationList[i].GuidedUploadMeditationId + '" value="' + GuidedUploadMeditationList[i].AverageRating + '" disable class="rating rating-loading" data-show-clear="false" data-show-caption="true" data-min="0" data-max="5" data-step="0.1">' +
                            '</div>' +
                        '</div>' +
                    '</a>' +
                    '</div>';
            }

        }
    }
    else {
        GuidedUploadMeditationHtml = '<div class="row ptb15"><div class="col-xs-12 text-center">No data found.</div></div>';
    }
    $('#DivGetRelatedGuidedUploadMeditation').html(GuidedUploadMeditationHtml);
}

function GoToViewGuided(GuidedUploadMeditationId) {
    window.sessionStorage.setItem("GuidedUploadMeditationId", GuidedUploadMeditationId);
    window.location.href = "viewguided.html";
}

function GetGuidedUploadMeditationDetailsCallback(result) {

    var GuidedUploadMeditationDetails = JSON.parse(result.Response.Data);

    if (GuidedUploadMeditationDetails.length > 0) {
        for (var i = 0; i < GuidedUploadMeditationDetails.length; i++) {
            var GuidedUploadMeditationHtml = '';
            var GuidedUploadMeditationMediaListHtml = '';

            $('#ViewGuidedTitle').html(GuidedUploadMeditationDetails[i].GuidedName);

            var likeClass = "text-success";
            if (GuidedUploadMeditationDetails[i].IsFavourite == false) {
                likeClass = "text-failure"
            }

            GuidedUploadMeditationHtml += '<script src="js/star-rating.min.js"></script>';
            GuidedUploadMeditationHtml +=
           '<div class="col-xs-12 text-center"><img src="' + GuidedUploadMeditationDetails[i].GuidedProfileImage + '" class="img-thumbnail" width="100px"></div>' +
           '<div class="col-xs-12 text-center">' +
               '<div class="mtb10"><b class="text-white">' + GuidedUploadMeditationDetails[i].Title + '</b></div>' +
               ' <div class="starrating" disabled>' +
                               '<input disabled id="input_' + GuidedUploadMeditationDetails[i].GuidedUploadMeditationId + '" value="' + GuidedUploadMeditationDetails[i].AverageRating + '" disable class="rating rating-loading" data-show-clear="false" data-show-caption="true" data-min="0" data-max="5" data-step="0.1">' +
               '</div>' +
               '<div class="mtb10 text-muted">' + GuidedUploadMeditationDetails[i].NoOfPlays + ' plays • ' + GuidedUploadMeditationDetails[i].TotalRating + ' ratings</div>' +
               '<div class="text-gray">' + GuidedUploadMeditationDetails[i].Description + '</div>' +
           '</div>' +
           '<a class="like" id="' + GuidedUploadMeditationDetails[i].GuidedUploadMeditationId + '" onclick="AddRemoveFavourites(this)"><i class="fa ' + likeClass + ' fa-heart fa-2x"></i></a>';

            $('#DivContactGuided').html(GuidedUploadMeditationDetails[i].ContactGuided);

            if (GuidedUploadMeditationDetails[i].GuidedUploadMeditationMediaList.length > 0) {
                var GuidedSoundFile = "",
                    fileType = "",
                    TrackLength = "",
                    icon = "fa-play-circle";


                for (var i1 = 0; i1 < GuidedUploadMeditationDetails[i].GuidedUploadMeditationMediaList.length; i1++) {

                    if (GuidedUploadMeditationDetails[i].GuidedUploadMeditationMediaList[i1].UploadAudioName.trim() != "") {

                        GuidedSoundFile = (GuidedAudioUrl + GuidedUploadMeditationDetails[i].GuidedUploadMeditationMediaList[i1].UploadAudioName);
                        fileType = "audio";
                        icon = "fa-play-circle";

                    } else if (GuidedUploadMeditationDetails[i].GuidedUploadMeditationMediaList[i1].VideoUrl.trim() != "") {

                        GuidedSoundFile = GuidedUploadMeditationDetails[i].GuidedUploadMeditationMediaList[i1].VideoUrl;
                        fileType = "video";
                        icon = "fa-file-video-o";

                    } else if (GuidedUploadMeditationDetails[i].GuidedUploadMeditationMediaList[i1].UploadPdfName.trim() != "") {
                        GuidedSoundFile = (GuidedPdfUrl + GuidedUploadMeditationDetails[i].GuidedUploadMeditationMediaList[i1].UploadPdfName);
                        fileType = "pdf";
                        icon = "fa-file-text-o";

                    }

                    TrackLength = GuidedUploadMeditationDetails[i].GuidedUploadMeditationMediaList[i1].TrackLength;

                    GuidedUploadMeditationMediaListHtml +=
                       '<div class="row">' +
                       '<div class="col-xs-5 fa-2x text-center mtb10">' + GuidedUploadMeditationDetails[i].GuidedUploadMeditationMediaList[i1].TrackName + '</div>' +
                       '<div class="col-xs-2 text-center text-success mtb10">\
                        <a class="text-success" onclick=UpdatePlayCount(' + GuidedUploadMeditationDetails[i].GuidedUploadMeditationId + ',' + GuidedUploadMeditationDetails[i].GuidedUploadMeditationMediaList[i1].GuidedUploadMeditationMediaId + ',' + GuidedUploadMeditationDetails[i].CategoryId + ',"' + GuidedSoundFile + '","' + fileType + '","' + TrackLength + '")>\
                        <i class="fa fa-3x ' + icon + '"></i></a></div>' +
                       '<div class="col-xs-5 fa-2x text-center mtb10">' + TrackLength + '</div>' +
                       '</div>' +
                       '<div style="border-bottom:1px solid #222;margin:10px -15px;"></div>';
                }
            }
            else {
                GuidedUploadMeditationMediaListHtml = "No Tracks Found.<div style='border-bottom:1px solid #222;margin:10px -15px;'></div>";

                $('#DivViewguidedMedia').addClass("text-center");
            }
        }
        $('#DivViewguided').html(GuidedUploadMeditationHtml);
        $('#DivViewguidedMedia').html(GuidedUploadMeditationMediaListHtml);
    }
}

function UpdatePlayCount(GuidedUploadMeditationId, GuidedUploadMeditationMediaId, CategoryId, GuidedSoundFile, fileType, TrackLength) {

    window.sessionStorage.setItem("GuidedUploadMeditationId", GuidedUploadMeditationId);
    window.sessionStorage.setItem("GuidedUploadMeditationMediaId", GuidedUploadMeditationMediaId);
    window.sessionStorage.setItem("CategoryId", CategoryId);
    window.sessionStorage.setItem("GuidedSoundFile", GuidedSoundFile);
    window.sessionStorage.setItem("FileType", fileType);

    if (TrackLength.trim().length > 0 && fileType == "video") {
        window.sessionStorage.setItem("TrackLength", TrackLength);
    }

    Data = {
        "GuidedUploadMeditationId": GuidedUploadMeditationId
    };
    callname = "UpdatePlayCount";
    callMindfulService(JSON.stringify(Data), callname);
}

function UpdatePlayCountCallback(result) {
    if (window.sessionStorage.getItem("FileType").trim() == "audio") {
        window.location.href = "guided_play.html";
    } else if (window.sessionStorage.getItem("FileType").trim() == "video") {
        window.location.href = "playvideo.html";

    } else if (window.sessionStorage.getItem("FileType").trim() == "pdf") {
        OpenPdfViewer();
    }
}

function UpdateMyUploadMeditationPlayCount(MyUploadMeditationId) {
    Data = {
        "MyUploadMeditationId": MyUploadMeditationId
    };
    callname = "UpdateMyUploadMeditationPlayCount";
    callMindfulService(JSON.stringify(Data), callname);
}

function UpdateMyUploadMeditationPlayCountCallback(result) {
    //var IsVedio = window.sessionStorage.getItem("IsVedio");
    //if (IsVedio == 1) {
    //    window.location.href = "playvideo.html";
    //}
    //else {
    //    window.location.href = "myuploadsmeditaionplay.html";
    //}

    window.sessionStorage.setItem("CategoryId", 7);

    if (window.sessionStorage.getItem("FileType").trim() == "audio") {
        window.location.href = "guided_play.html";
    } else if (window.sessionStorage.getItem("FileType").trim() == "video") {
        window.location.href = "playvideo.html";
    } else if (window.sessionStorage.getItem("FileType").trim() == "pdf") {
        OpenPdfViewer();
    }
}

function GetUserRatingCallback(result) {
    var UserRating = JSON.parse(result.Response.Data);
    if (UserRating.length > 0) {
        var UserRatingHTML = '';
        for (var i = 0; i < UserRating.length; i++) {
            UserRatingHTML += '<script src="js/star-rating.min.js"></script>';
            UserRatingHTML +=
            '<div class="row mtb15">' +
            '<div class="col-xs-3"><img src="' + UserRating[i].ProfileImage + '" class="img-thumbnail" width="100%"></div>' +
            '<div class="col-xs-9" style="padding-left:0;">' +
	            ' <div class="starrating" disabled>' +
                               '<input disabled id="input_' + UserRating[i].GuidedUploadMeditationId + '" value="' + UserRating[i].Rate + '" disable class="rating rating-loading" data-show-clear="false" data-show-caption="true" data-min="0" data-max="5" data-step="0.1">' +
               '</div>' +
	            '<div class="text-muted"><b class="text-success">' + UserRating[i].UserName + ' - </b> ' + UserRating[i].Comment + '</div>' +
            '</div>' +
            '</div>';
        }
    }
    else {
        UserRatingHTML = '<div class="row mtb15"><div class="col-xs-12 text-center">No reviews yet.</div></div>';
    }
    $('#DivUserRating').html(UserRatingHTML);
}

function ManageUserRatingCallback(result) {
    //alert("Rating success.");
    //window.location.href = "viewguided.html";
    GoBackToSubCategory();
}

//rate for neurominutes track details
function AddNeurominReview() {
    var trackDetailId = window.sessionStorage.getItem("TrackDetailId");

    Data = {
        "model": {
            UserId: userData.Id,
            NeuroMinTrackId: window.sessionStorage.getItem("TrackId"),
            NeuroMinTrackDetailId: trackDetailId,
            Rate: $('#spnRateCaption').text(),
            Comment: $('#txtComment').val()
        }
    };

    callname = "AddNeurominReview";
    callMindfulService(JSON.stringify(Data), callname);
}

function AddNeurominReviewCallback(result) {
    //alert(result.Response.Message);    
    if (result.Response.Result) {
        window.location.href = "neurosynctracks_view.html";
    }
}
//Neurominutes Tracks Detail end

function GetAllGuidedMeditationCategoryDropDownCallback(result) {
    var GuidedMeditationCategoryList = JSON.parse(result.Response.Data);
    if (GuidedMeditationCategoryList.length > 0) {
        var CategoryHtml = '';

        for (var i = 0; i < GuidedMeditationCategoryList.length; i++) {
            //console.log("title :" + GuidedMeditationCategoryList[i].CategoryName);
            CategoryHtml += '<option value="' + GuidedMeditationCategoryList[i].CategoryId + '">';
            CategoryHtml += GuidedMeditationCategoryList[i].CategoryName + '</option>';
        }

        $('#ddlCategory').html(CategoryHtml);
    }
}

function InsertGuidedMeditationDataCallback(result) {
    if (result.Response.Result) {
        window.location.href = "guided.html";
    }
}

function InsertUpdateMyUploadsCallback(result) {
    if (result.Response.Result) {
        window.location.href = "myuploadsmeditaion.html";
    }
}

function GetAllMyUploadCallback(result) {
    var list = JSON.parse(result.Response.Data);

    if (list.length > 0) {
        var Html = '';
        var GuidedSoundFile = "",
                    fileType = "";

        for (var i = 0; i < list.length; i++) {
            var icon = list[i].UploadPdfName == "" ? "fa-play-circle" : "fa-file-text-o";
            var IsVedio = 0;

            if (list[i].UploadAudioName.trim() != "") {
                GuidedSoundFile = (GuidedAudioUrl + list[i].UploadAudioName);
                fileType = "audio";
            } else if (list[i].VideoUrl.trim() != "") {
                GuidedSoundFile = list[i].VideoUrl;
                fileType = "video";
            } else if (list[i].UploadPdfName.trim() != "") {
                GuidedSoundFile = (GuidedPdfUrl + list[i].UploadPdfName);
                fileType = "pdf";
            }

            if (list[i].VideoUrl != "" && list[i].VideoUrl != null) {
                window.sessionStorage.setItem("VideoUrl", list[i].VideoUrl);
                icon = "fa fa-file-video-o";
                IsVedio = 1;
            }

            Html += '<div class="row" style="border-bottom: 1px solid #222;">';
            Html += '<a href="javascript:void(0);" onclick=opendisclaimerPopup(' + list[i].MyUploadMeditationId + ',' + IsVedio + ',"' + GuidedSoundFile + '","' + fileType + '")>';
            Html += '<div class="col-xs-3 text-center ptb15"><i class="fa fa-5x ' + icon + '"></i></div>';
            Html += '<div class="col-xs-9 plr0 ptb15">';
            Html += '<div class="mt10"><b class="text-white">' + list[i].Title + '</b></div>';
            Html += '<div class="mt5 text-muted"><i class="fa fa-group"></i> ' + list[i].ViewCount + ' views</div>'
            Html += '</div></a></div>';
        }

        $('#DivContain').html(Html);
    }
}

function opendisclaimerPopup(MyUploadMeditationId, IsVedio, GuidedSoundFile, fileType) {
    window.sessionStorage.setItem("MyUploadMeditationId", MyUploadMeditationId);
    window.sessionStorage.setItem("IsVedio", IsVedio);
    window.sessionStorage.setItem("GuidedSoundFile", GuidedSoundFile);
    window.sessionStorage.setItem("FileType", fileType);

    $("#disclaimer").modal("show");

}

function GetGuidedUploadMeditationMediaListCallback(result) {
    var GuidedUploadMeditationDetails = JSON.parse(result.Response.Data);
    if (GuidedUploadMeditationDetails.length > 0) {
        var GuidedUploadMeditationHtml = '';
        var GuidedSoundFile = "",
            fileType = "",
            TrackLength = "";

        GuidedUploadMeditationHtml += '<div class="row" style="border-bottom: 1px solid #222;">\
                    <a data-toggle="modal" data-target="#terms">\
                        <div class="col-xs-12 mtb10">\
                            <h5 class="mt0 mb0 text-right">about this section</h5>\
                        </div>\
                    </a>\
                </div>';

        for (var i = 0; i < GuidedUploadMeditationDetails.length; i++) {

            if (GuidedUploadMeditationDetails[i].UploadAudioName.trim() != "") {
                GuidedSoundFile = (GuidedAudioUrl + GuidedUploadMeditationDetails[i].UploadAudioName);
                fileType = "audio";
            } else if (GuidedUploadMeditationDetails[i].VideoUrl.trim() != "") {
                GuidedSoundFile = GuidedUploadMeditationDetails[i].VideoUrl;
                fileType = "video";
            } else if (GuidedUploadMeditationDetails[i].UploadPdfName.trim() != "") {
                GuidedSoundFile = (GuidedPdfUrl + GuidedUploadMeditationDetails[i].UploadPdfName);
                fileType = "pdf";
            }

            var icon = GuidedUploadMeditationDetails[i].UploadPdfName == "" ? "fa-play-circle" : "fa-file-text-o";
            var IsVedio = 0;

            if (GuidedUploadMeditationDetails[i].VideoUrl != "" && GuidedUploadMeditationDetails[i].VideoUrl != null) {
                window.sessionStorage.setItem("VideoUrl", GuidedUploadMeditationDetails[i].VideoUrl);
                icon = "fa fa-file-video-o";
                IsVedio = 1;
            }

            TrackLength = "";
            TrackLength = GuidedUploadMeditationDetails[i].TrackLength;

            GuidedUploadMeditationHtml +=
            '<div class="row" style="border-bottom: 1px solid #222;">' +
                '<a href="javascript:void(0);" onclick=UpdateMediaPlayCount(' + GuidedUploadMeditationDetails[i].GuidedUploadMeditationId + ',' + GuidedUploadMeditationDetails[i].GuidedUploadMeditationMediaId + ',"' + GuidedSoundFile + '","' + fileType + '","' + TrackLength + '")>' +
                '<div class="col-xs-3 text-center ptb15"><i class="fa fa-5x ' + icon + '"></i></div>' +
                '<div class="col-xs-9 plr0 ptb15">' +
                    '<div class="mt10"><b class="text-white">' + GuidedUploadMeditationDetails[i].TrackName + '</b></div>' +
                    '<div class="mt5 text-muted"><i class="fa fa-group"></i> ' + GuidedUploadMeditationDetails[i].NoOfPlays + ' views</div>' +
                '</div>' +
                '</a>' +
            '</div>';
        }

    } else {
        GuidedUploadMeditationHtml = '<div class="row mtb15"><div class="col-xs-12 text-center">No data found.</div></div>';
    }
    $('#DivMeditationDetailsContain').html(GuidedUploadMeditationHtml);
}

function UpdateMediaPlayCount(GuidedUploadMeditationId, GuidedUploadMeditationMediaId, GuidedSoundFile, fileType, TrackLength) {

    window.sessionStorage.setItem("GuidedUploadMeditationId", GuidedUploadMeditationId);
    window.sessionStorage.setItem("GuidedUploadMeditationMediaId", GuidedUploadMeditationMediaId);
    window.sessionStorage.setItem("GuidedSoundFile", GuidedSoundFile);
    window.sessionStorage.setItem("FileType", fileType);
    window.sessionStorage.setItem("TrackLength", TrackLength);

    $("#disclaimer").modal("show");
}

function UpdateMyUploadMeditationMediaPlayCount(GuidedUploadMeditationId, IsVedio) {

    Data = {
        "GuidedUploadMeditationId": GuidedUploadMeditationId
    };
    callname = "UpdatePlayCount";
    callMindfulService(JSON.stringify(Data), callname);
}

function OpenPdfViewer() {
    var pdfUrl = encodeURI('http://docs.google.com/viewer?url=' + window.sessionStorage.getItem("GuidedSoundFile"));
    //var pdfUrl = (window.sessionStorage.getItem("GuidedSoundFile"));
    //var ref = window.open(encodeURI('http://docs.google.com/viewer?url=http://www.pdf995.com/samples/pdf.pdf'), '_blank', 'location=yes');
    var ref = window.open(pdfUrl, '_blank', 'location=no');

    var myCallback = function () { alert(event.url); }
    //ref.addEventListener('loadstart', myCallback);
    //ref.removeEventListener('loadstart', myCallback);
}
function GetTalkInterviewListCallback(result) {
    var TalkInterviewList = JSON.parse(result.Response.Data);
    if (TalkInterviewList.length > 0) {
        var TalkInterviewListHtml = '';
        var GuidedSoundFile = "",
                   fileType = "",
                   TrackLength = "",
                   icon = "fa-play-circle";
        //TalkInterviewListHtml += '<script src="js/star-rating.min.js"></script>';
        for (var i = 0; i < TalkInterviewList.length; i++) {
            if (TalkInterviewList[i].AudioFileName != "" && TalkInterviewList[i].AudioFileName != null) {
                GuidedSoundFile = (GuidedAudioUrl + TalkInterviewList[i].AudioFileName);
                fileType = "audio";
                icon = "fa-play-circle";
            } else if (TalkInterviewList[i].VideoUrl != "" && TalkInterviewList[i].VideoUrl != null) {
                GuidedSoundFile = TalkInterviewList[i].VideoUrl;
                fileType = "video";
                icon = "fa-file-video-o";
            } else if (TalkInterviewList[i].PdfName != "" && TalkInterviewList[i].PdfName != null) {
                GuidedSoundFile = (GuidedPdfUrl + TalkInterviewList[i].PdfName);
                fileType = "pdf";
                icon = "fa-file-text-o";
            }
            TalkInterviewListHtml +=
                    '<div  class="row" style="border-bottom: 1px solid #222;">' +
                        '<a href="" data-toggle="modal" data-target="#disclaimer" onclick=PlayTalksAndInterviewSound("' + GuidedSoundFile + '",' + TalkInterviewList[i].TalksAndInterviewsId + ',"' + fileType + '")>' +
                            '<div class="col-xs-3 text-center ptb15"><i class="fa fa-5x ' + icon + '" ></i></div>' +
                            '<div class="col-xs-9 plr0 ptb15">' +
                                '<div class="mt10"><b class="text-white">' + TalkInterviewList[i].Title + '</b></div>' +
                                '<div class="mt5 text-muted"><i class="fa fa-group"></i> ' + TalkInterviewList[i].ViewCount + ' views</div>' +
                            '</div>' +
                        '</a>' +
                    '</div>';
        }
    }
    else {
        TalkInterviewListHtml = '<div class="row ptb15"><div class="col-xs-12 text-center">No data found.</div></div>';
    }
    $('#DivTalksandInterviewsList').html(TalkInterviewListHtml);
}
function UpdateTalkInterviewPlayCount(TalksAndInterviewsId) {
    Data = {
        "TalksAndInterviewsId": TalksAndInterviewsId
    };
    callname = "UpdateTalksAndInterviewsPlayCount";//service sp name
    callMindfulService(JSON.stringify(Data), callname);
}
function UpdateTalksAndInterviewsPlayCountCallback(result) {
    if (window.sessionStorage.getItem("FileType") == "audio") {
        window.location.href = "TalksAndInterviews_play.html";
    } else if (window.sessionStorage.getItem("FileType") == "video") {
        window.location.href = "TalksAndInterviewsVideo.html";
    } else if (window.sessionStorage.getItem("FileType") == "pdf") {
        window.sessionStorage.setItem('GuidedSoundFile', window.sessionStorage.getItem("SoundFile"));
        OpenPdfViewer();
    }
}
function GetDivWritingsListCallback(result) {
    var DivWritingsList = JSON.parse(result.Response.Data);
    if (DivWritingsList.length > 0) {
        var WritingsListHtml = '';
        var GuidedSoundFile = "",
                   fileType = "",
                   TrackLength = "";
        for (var i = 0; i < DivWritingsList.length; i++) {
            if (DivWritingsList[i].PdfName != "" && DivWritingsList[i].PdfName != null) {
                GuidedSoundFile = (GuidedPdfUrl + DivWritingsList[i].PdfName);
                fileType = "pdf";
                icon = "fa-file-text-o";
            }
            WritingsListHtml +=
                 '<div class="row" style="border-bottom: 1px solid #222;">' +
                    '<a href="" data-toggle="modal" data-target="#disclaimer"  onclick=PlayTalksAndInterviewSound("' + GuidedSoundFile + '",' + DivWritingsList[i].UserUploadWritingsId + ',"' + fileType + '")>' +
                        '<div class="col-xs-3 text-center ptb15"><i class="fa fa-5x fa-file-text-o"></i></div>' +
                        '<div class="col-xs-9 plr0 ptb15">' +
                            '<div class="mt10"><b class="text-white">' + DivWritingsList[i].Title + '</b></div>' +
                            '<div class="mt5 text-muted"><i class="fa fa-group"></i> ' + DivWritingsList[i].ViewCount + ' views</div>' +
                        '</div>' +
                    '</a>' +
                '</div>';
        }
    }
    else {
        WritingsListHtml = '<div class="row ptb15"><div class="col-xs-12 text-center">No data found.</div></div>';
    }
    $('#DivWritings').html(WritingsListHtml);
}
function UpdateWritingsViewCount(UserUploadWritingsId) {
    Data = {
        "UserUploadWritingsId": UserUploadWritingsId
    };
    callname = "UpadateUserUploadWritingsViewCount";//service sp name
    callMindfulService(JSON.stringify(Data), callname);
}
function UpdateWritingsViewCountCallback(result) {
    if (window.sessionStorage.getItem("FileType") == "pdf") {
        OpenPdfViewer();
    }
}
function GetDivUserUploadResourcesListCallback(result) {

    var UserUploadResourcesList = JSON.parse(result.Response.Data);

    if (UserUploadResourcesList.length > 0) {

        var UserUploadResourcesListHtml = '';
        var GuidedSoundFile = "",
                   fileType = "",
                   TrackLength = "",
                   icon = "fa-play-circle";

        for (var i = 0; i < UserUploadResourcesList.length; i++) {

            if (UserUploadResourcesList[i].AudioFileName != "" && UserUploadResourcesList[i].AudioFileName != null) {
                GuidedSoundFile = (ResourceAudioUrl + UserUploadResourcesList[i].AudioFileName);
                fileType = "audio";
                icon = "fa-play-circle";
            } else if (UserUploadResourcesList[i].PdfName != "" && UserUploadResourcesList[i].PdfName != null) {
                GuidedSoundFile = (ResourcePdfUrl + UserUploadResourcesList[i].PdfName);
                fileType = "pdf";
                icon = "fa-file-text-o";
            } else if (UserUploadResourcesList[i].VideoUrl != "" && UserUploadResourcesList[i].VideoUrl != null) {
                GuidedSoundFile = UserUploadResourcesList[i].VideoUrl;
                fileType = "video";
                icon = "fa-file-video-o";
            }

            UserUploadResourcesListHtml +=
                  '<div class="row" style="border-bottom: 1px solid #222;">' +
                    '<a href="javascript:void(0);" onclick=PlayUserUploadResourcesSound("' + GuidedSoundFile + '",' + UserUploadResourcesList[i].UserUploadResourceId + ',"' + fileType + '")>' +
                        '<div class="col-xs-3 text-center ptb15"><i class="fa fa-5x ' + icon + '"></i></div>' +
                        '<div class="col-xs-9 plr0 ptb15">' +
                            '<div class="mt10"><b class="text-white">' + UserUploadResourcesList[i].Title + '</b></div>' +
                            '<div class="mt5 text-muted"><i class="fa fa-group"></i> ' + UserUploadResourcesList[i].ViewCount + ' views</div>' +
                        '</div>' +
                    '</a>' +
                '</div>';
        }
    }
    else {
        UserUploadResourcesListHtml = '<div class="row ptb15"><div class="col-xs-12 text-center">No data found.</div></div>';
    }
    $('#DivMyUploads').html(UserUploadResourcesListHtml);
}
function UpdateUserUploadResourcesViewCount(UserUploadResourceId) {
    Data = {
        "UserUploadResourceId": UserUploadResourceId
    };
    callname = "UpadateUserUploadResourcesViewCount";//service sp name
    callMindfulService(JSON.stringify(Data), callname);
}

function UpdateUserUploadResourcesViewCountCallback(result) {
    if (window.sessionStorage.getItem("FileType") == "audio") {
        window.location.href = "MyUploadResourcePlay.html";
    } else if (window.sessionStorage.getItem("FileType") == "pdf") {
        window.sessionStorage.setItem('GuidedSoundFile', window.sessionStorage.getItem("SoundFile"));
        OpenPdfViewer();
    } else if (window.sessionStorage.getItem("FileType") == "video") {
        window.location.href = "MyUploadResourceVideo.html";
    }
}

function UserUploadResourcesVideoCallback(result) {
    window.location.href = "myuploads.html";
}
function GetResourceGroupListCallback(result) {
    var TalkInterviewList = JSON.parse(result.Response.Data);

    if (TalkInterviewList.length > 0) {

        var TalkInterviewListHtml = '';

        for (var i = 0; i < TalkInterviewList.length; i++) {

            TalkInterviewListHtml +=
                '<div class="mtb20"></div>' +
                '<div class="row">' +
                    '<a href="javascript:void(0);" onclick=ViewGroupDetails("' + TalkInterviewList[i].ResourceGroupId + '")>' +
                        '<div class="col-xs-4"><img src="' + TalkInterviewList[i].GroupImage + '" width="100%" class="img-thumbnail"></div>' +
                        '<div class="col-xs-8" style="padding-left:0;">' +
                            '<div><b class="text-primary">' + TalkInterviewList[i].GroupName + '</b></div>' +
                            '<div class="text-muted mt5">' + TalkInterviewList[i].GroupDescription + '</div>' +
                            '<div class="text-gray mt5">' +
                                '<i class="fa fa-group"></i> ' + (TalkInterviewList[i].TotalMembers + 1) +
                                '<span class="plr15"></span>' +
                                '<i class="fa fa-comments"></i> ' + TalkInterviewList[i].TotalMessages +
                            '</div>' +
                        '</div>' +
                    '</a>' +
                '</div>';
        }
    }
    else {
        TalkInterviewListHtml = '<div class="row ptb15"><div class="col-xs-12 text-center">No group found.</div></div>';
    }

    $('#DivPremium_Group').html(TalkInterviewListHtml);
}

/* Add Group Get Recipient List Start */
function GetRecipientList() {
    Data = {
        Type: "All",
        UserId: userData.Id,
        GroupId: 0
    };

    callname = "GetRecipientList";
    callMindfulService(JSON.stringify(Data), callname);
}

function GetRecipientListCallback(result) {
    var recipientList = JSON.parse(result.Response.Data);
    var recipientHtml = "";

    if (JSON.parse(result.Response.Message.trim() == "All")) {

        if (recipientList.length > 0) {

            for (var i = 0; i < recipientList.length; i++) {

                if (i == 0) {
                    recipientHtml += '<div class="input-group">\
                                <input type="text" class="form-control" value="All Contacts" readonly>\
                                <span class="input-group-addon"><input type="checkbox" id="CheckAll" name="chkAll" value="0"></span>\
                             </div>';
                } else {
                    recipientHtml += '<div class="input-group">\
                                <input type="text" class="form-control" readonly value="' + recipientList[i].RecipientName + '">\
                                <span class="input-group-addon"><input type="checkbox" name="chkRecipient"  value="' + recipientList[i].RecipientId + '"></span>\
                             </div>';
                }
            }

            $('#filter').html(recipientHtml);

            $('#CheckAll').click(function () {
                $("#filter .input-group input[name=chkRecipient]").prop('checked', this.checked);
            });

            $("#filter .input-group input[name=chkRecipient]").click(function () {
                var flag = true;

                $("#filter .input-group input[name=chkRecipient]").each(function () {
                    if ($(this).prop("checked") == false)
                        flag = false;
                });
                $("#CheckAll").prop('checked', flag);
            });

        } else {
            $('#btnRecipientList').val("No Recipient Available");
            $('#btnRecipientList').removeAttr('data-toggle');
            $('#btnRecipientList').removeAttr('data-target');
        }
    } else if (JSON.parse(result.Response.Message.trim() == "None")) {

        recipientHtml = "";

        if (recipientList.length > 0) {
            for (var i = 0; i < recipientList.length; i++) {

                if (i == 0) {
                    cssmtb15 = "";
                } else { cssmtb15 = " mtb15"; }

                recipientHtml += '<a data-dismiss="modal" href="javascript:void(0);" id="btnRequest' + recipientList[i].RecipientId + '" onclick="SendRequest(\'' + recipientList[i].RecipientId + '\',\'' + $("#DivGroupTitle").text().trim() + '\')">\
                        <div class="row'+ cssmtb15 + '">\
                            <div class="col-xs-3"><img src="' + recipientList[i].ProfileImage + '" class="img-thumbnail" width="100%"></div>\
                            <div class="col-xs-9 mtb10" style="padding-left:0;">\
                                <div><b class="text-primary">' + recipientList[i].RecipientName + '</b> in ' + recipientList[i].Location + '</div>\
                                <div class="text-muted"><small>about ' + recipientList[i].RegisterDate + '</small></div>\
                            </div>\
                        </div>\
                    </a>';

            }
        } else {
            recipientHtml = "";
        }

        $('#friendsList').html(recipientHtml);
    }
}

function SendRequest(RequestId, GroupName) {
    Data = {
        GroupId: window.sessionStorage.getItem("GroupId"),
        UserId: RequestId,
        GroupName: GroupName,
        InvititationFrom: userData.Id
    };

    callname = "SendInvitationToJoinGroup";
    callMindfulService(JSON.stringify(Data), callname);
}

function SendInvitationToJoinGroupCallback(result) {
    showAlert(result.Response.Message, "info");
}

/* Add Group Get Recipient List End */

/* View Group  Details Start */
function ViewGroupDetails(GroupId) {
    window.sessionStorage.setItem("GroupId", GroupId);
    window.location.href = "premium_groups_view.html";
}

function GetResourceGroupDetails() {

    Data = {
        GroupId: window.sessionStorage.getItem("GroupId"),
        UserId: userData.Id
    };

    callname = "GetResourceGroupDetails";
    callMindfulService(JSON.stringify(Data), callname);
}

function GetResourceGroupDetailsCallback(result) {

    var resourceGroup = JSON.parse(result.Response.Data);
    var GroupMessageHtml = "";

    $("#DivGroupTitle").html(resourceGroup.GroupName);
    $('#DivDescription').html(resourceGroup.GroupDescription);
    $('#DivInformation').html(resourceGroup.GroupInformation);
    $("#imgGroupIcon").attr("src", resourceGroup.GroupImage);

    $("input[name=cbGroupSettings]").attr("data-respourcegroupid", resourceGroup.ResourceGroupId);
    $("#cbShowEveryoneTab").prop("checked", resourceGroup.ShowMeToAll);
    $("#cbShowFriendsTab").prop("checked", resourceGroup.ShowMeToFriends);
    $("#cbNotifyMe").prop("checked", resourceGroup.NotifyMe);

    if (resourceGroup.GroupCreatedBy == userData.Id) {
        $("#btnJoinGroup").addClass("hidden");
        $('#DivGroupSettings').removeClass("hidden");
        $('#btnInviteeList').removeClass("hidden");

        GroupMessageHtml += '<div class="col-xs-12 mtb20 text-center">\
								<button class="btn btn-md btn-success text-uppercase" onclick=postMessage("' + resourceGroup.ResourceGroupId + '")>Post Message</button>\
							</div>';

    } else {
        $("#btnJoinGroup").removeClass("hidden");
        $("#btnJoinGroup").attr("value", resourceGroup.ResourceGroupId);
        $("#btnJoinGroup").attr("data-isrequestaccepted", !resourceGroup.IsRequestAccepted);

        if (resourceGroup.IsRequestAccepted) {
            $("#btnJoinGroup").html("Leave Group");
            $('#DivGroupSettings').removeClass("hidden");
            $('#btnInviteeList').removeClass("hidden");

            //five days app use condition pending
            if (!resourceGroup.IsActive) {

                GroupMessageHtml += "<div class='alert alert-warning mtb15 text-center' role='alert'>You may post messages after you've used the app for at least five days.</div>";

            } else {
                GroupMessageHtml += '<div class="col-xs-12 mtb20 text-center">\
								<button class="btn btn-md btn-success text-uppercase" onclick=postMessage("' + resourceGroup.ResourceGroupId + '")>Post Message</button>\
							</div>';
            }

        } else {
            $("#btnJoinGroup").html("Join Group");
            $('#DivGroupSettings').addClass("hidden");
            GroupMessageHtml += "<div class='alert alert-warning mtb15 text-center' role='alert'>You may post messages after you've used the app for at least five days.</div>";
        }
    }

    /* ---------- Messages Start -------------*/

    if (resourceGroup.ResourceGroupMessageList != null && resourceGroup.ResourceGroupMessageList.length > 0) {

        for (var i = 0; i < resourceGroup.ResourceGroupMessageList.length; i++) {

            var messages = resourceGroup.ResourceGroupMessageList[i];
            var LikeClickHtml = "", LikedCss = "", CommentClickHtml = "";

            if (resourceGroup.IsRequestAccepted || resourceGroup.GroupCreatedBy == userData.Id) {

                if (messages.IsLiked) {
                    LikeClickHtml = "";
                    LikedCss = " text-success";
                } else {
                    LikeClickHtml = "onclick=UpdateLikesToGroupMessage('" + messages.ResourceGroupMsgId + "')";
                    LikedCss = "";
                }

                CommentClickHtml = "onclick=UpdateCommentsToGroupMessage('" + messages.ResourceGroupMsgId + "')";
            }

            GroupMessageHtml += '<div class="row mtb15" style="border-bottom:1px solid #222;">\
                                <div class="col-xs-3"><img src="' + messages.ProfileImage + '" class="img-thumbnail" width="100%"></div>\
                                <div class="col-xs-9 mtb10" style="padding-left:0;">\
                                    <div><b class="text-primary">' + messages.Name + '</b></div>\
                                    <div class="text-muted"><small>' + messages.InsertDateTime + ' in ' + messages.Location + '</small></div>\
                                </div>\
                                <div class="col-xs-12 mtb10">\
                                    <div>' + messages.Message + '</div>\
                                    <div class="text-muted mtb10">\
                                        <a href="javascript:void(0);" id="btnlike' + messages.ResourceGroupMsgId + '" ' + LikeClickHtml + '><i class="fa' + LikedCss + ' fa-heart"></i> ' + messages.TotalLikes + ' likes</a>\
                                        <span class="plr15"></span>\
                                        <a href="javascript:void(0);" id="btncomment' + messages.ResourceGroupMsgId + '" ' + CommentClickHtml + '><i class="fa fa-comment-o"></i> ' + messages.TotalComments + ' comments</a>\
                                    </div>\
                                </div>\
                            </div>';
        }
    } else {
        GroupMessageHtml += '<div class="row mt15"><div class="col-xs-12 text-center">No Message Found.</div></div>';
    }
    $("#GroupMessage").html(GroupMessageHtml);
    /* ---------- Messages End -------------*/

    /* ---------- Members Start -------------*/
    var GroupMembersHtml = "";
    if (resourceGroup.ResourceGroupMemberList != null && resourceGroup.ResourceGroupMemberList.length > 0) {

        for (var i = 0; i < resourceGroup.ResourceGroupMemberList.length; i++) {

            var members = resourceGroup.ResourceGroupMemberList[i];

            if (members.IsOwner) {

                var tagLine = "";

                if (members.TagLine != "") {
                    tagLine = '<i class="fa fa-star text-success"></i> <small>' + members.TagLine + '</small>';
                }

                GroupMembersHtml += '<div class="row alert-success" style="border-radius:0;">\
                                <div class="mtb15" style="border-radius:0;">\
                                    <div class="col-xs-3"><img src="' + members.ProfileImage + '" class="img-thumbnail" width="100%"></div>\
                                    <div class="col-xs-9" style="padding-left:0;">\
                                        <div class="text-muted"><small>Created by</small></div>\
                                        <div><b class="text-primary">' + members.Name + '</b> in ' + members.Location + '</div>\
                                        <div>' + tagLine + '</div>\
                                        <div class="text-muted"><small>' + members.InsertDateTime + '</small></div>\
                                    </div>\
                                    <div class="clearfix"></div>\
                                </div>\
                            </div>';
            } else {

                GroupMembersHtml += '<div class="row mtb15">\
                                <div class="col-xs-3"><img src="'+ members.ProfileImage + '" class="img-thumbnail" width="100%"></div>\
                                <div class="col-xs-9 mtb10" style="padding-left:0;">\
                                    <div><b class="text-primary">' + members.Name + '</b> in ' + members.Location + '</div>\
                                    <div class="text-muted"><small>about ' + members.InsertDateTime + '</small></div>\
                                </div>\
                            </div>';
            }
        }
    } else {
        GroupMembersHtml = '<div class="row mt15"><div class="col-xs-12 text-center">No Member Found.</div></div>';
    }
    $("#GroupMembers").html(GroupMembersHtml);
    /* ---------- Members End -------------*/
}

/*-------------- Update Likes To Group Message Start --------------*/
function UpdateLikesToGroupMessage(ResourceGroupMsgId) {

    window.sessionStorage.setItem("ResourceGroupMsgId", ResourceGroupMsgId);

    Data = {
        ResourceGroupMsgId: ResourceGroupMsgId,
        UserId: userData.Id
    };

    callname = "AddLikesToGroupMessage";
    callMindfulService(JSON.stringify(Data), callname);
}

function AddLikesToGroupMessageCallback(result) {

    var ResourceGroupMsgId = window.sessionStorage.getItem("ResourceGroupMsgId");
    $("#btnlike" + ResourceGroupMsgId).html('<i class="fa text-success fa-heart"></i> ' + JSON.parse(result.Response.Data) + " likes");
    $("#btnlike" + ResourceGroupMsgId).removeAttr('onclick');

}
/*-------------- Update Likes To Group Message End--------------*/

/*-------------- Update Comments To Group Message Start--------------*/
function UpdateCommentsToGroupMessage(ResourceGroupMsgId) {

    window.sessionStorage.setItem("ResourceGroupMsgId", ResourceGroupMsgId);

    window.location.href = "premium_groups_post_comment.html";
}
function PostNewComment() {

    if ($("#txtMessageComment").val().trim().length > 0) {
        $('#txtMessageComment').removeClass("has-error");

        Data = {
            ResourceGroupMsgId: window.sessionStorage.getItem("ResourceGroupMsgId"),
            UserId: userData.Id,
            Comment: $("#txtMessageComment").val()
        };

        callname = "AddCommentsToGroupMessage";
        callMindfulService(JSON.stringify(Data), callname);
    } else {
        $('#txtMessageComment').addClass("has-error");
        return false;
    }
}
/*-------------- Update Comments To Group Message End--------------*/

/*-------------- Post A Message Start -------------------------------*/
function postMessage(GroupId) {
    window.sessionStorage.setItem("GroupId", GroupId);
    window.location.href = "premium_groups_post_message.html";
}

function PostNewMessage() {

    if ($("#txtGroupMessage").val().trim().length > 0) {
        $('#txtGroupMessage').removeClass("has-error");

        Data = {
            GroupId: window.sessionStorage.getItem("GroupId"),
            UserId: userData.Id,
            Message: $("#txtGroupMessage").val()
        };

        callname = "AddMessageToGroup";
        callMindfulService(JSON.stringify(Data), callname);
    } else {
        $('#txtGroupMessage').addClass("has-error");
        return false;
    }
}
/*-------------- Post A Message End -------------------------------*/

/*-------------- Add / Remove Member From Group Start -------------------------------*/
function AddRemoveMemberFromGroup(GroupId, UserId, IsRequestAccepted) {

    window.sessionStorage.setItem("GroupId", GroupId);

    Data = {
        GroupId: GroupId,
        UserId: UserId,
        IsRequestAccepted: IsRequestAccepted
    };

    callname = "AddRemoveMemberFromGroup";
    callMindfulService(JSON.stringify(Data), callname);
}
function AddRemoveMemberFromGroupCallback(result) {
    window.location.href = "premium_groups_view.html";
}
/*-------------- Add / Remove Member From Group End -------------------------------*/
/* View Group  Details End */

function GetMessageGroupListCallback(result) {
    var MessageGroupList = JSON.parse(result.Response.Data);
    if (MessageGroupList.length > 0) {
        var MessageGroupListHtml = '';
        var GuidedSoundFile = "",
                   fileType = "",
                   TrackLength = "",
                   icon = "fa-play-circle";
        for (var i = 0; i < MessageGroupList.length; i++) {
            MessageGroupListHtml +=
                '<div class="mtb10"></div>' +
                '<div class="row">' +
                    '<a href="viewgroups.html" onclick=\"GetMsgCenterId(' + MessageGroupList[i].MsgCenterGroupId + ')\" >' +
                        '<div class="col-xs-4"><img src="' + MessageGroupList[i].GroupImage + '" width="100%" class="img-thumbnail"></div>' +
                        '<div class="col-xs-8" style="padding-left:0;">' +
                            '<div><b class="text-white">' + MessageGroupList[i].GroupName + '</b></div>' +
                            '<div class="text-muted">' + MessageGroupList[i].GroupDescription + '</div>' +
                            '<div class="mtb10 row text-gray">' +
                                '<span class="col-xs-6"><i class="fa fa-group"></i> ' + MessageGroupList[i].TotalMembers + '</span>' +
                                '<span class="col-xs-6"><i class="fa fa-commenting"></i> ' + MessageGroupList[i].TotalMessages + '</span>' +
                            '</div>' +
                        '</div>' +
                    '</a>' +
                '</div>' +
              '<div style="margin:15px;"></div>';
        }
    }
    else {
        MessageGroupListHtml = '<div class="row ptb15"><div class="col-xs-12 text-center">No data found.</div></div>';
    }
    $('#DivMessageGroups').html(MessageGroupListHtml);
}
function GetMsgCenterGroupByIdCallback(result) {
    var MessageGroupList = JSON.parse(result.Response.Data);
    if (MessageGroupList != null) {
        var Image = '';
        var DivDital = '';
        Image +=
        '<img src="' + MessageGroupList.GroupImage + '" class="img-thumbnail" width="100px">';
        $('#DivImge').html(Image);
        DivDital +=
            '<div class="col-xs-12">' +
                '<p><b>Description:</b></p>' +
                '<p>' + MessageGroupList.GroupDescription + '</p>' +
            '</div>' +
            '<div class="col-xs-12 mtb15">' +
                '<p><b>Addition Information:</b></p>' +
                '<p align="justify">' + MessageGroupList.GroupInformation + '</p>' +
                '</div>';
        $('#DivDetails').html(DivDital);
        $('#ViewGrouptitle').text(MessageGroupList.GroupName);
    }
    window.sessionStorage.setItem('IsGroupJoined', MessageGroupList.IsGroupJoined);
    if (MessageGroupList.IsGroupJoined == true) {
        $('#btnJoinGroup').hide();
        $('#btnLeavegroup').show();
        $('#DivMessageInfo').hide();
    }
    else {
        $('#btnJoinGroup').show();
        $('#btnLeavegroup').hide();
        $('#DivMessageInfo').show();
    }
    var MessageGroupListHtml = '';
    if (MessageGroupList.MessageCenterGroupsModelList.MessageCenterGroupMessageList.length > 0) {
        var cssstyle = '';
        var GuidedSoundFile = "",
                   fileType = "",
                   TrackLength = "",
                   icon = "fa-play-circle";
        if (MessageGroupList.IsGroupJoined == false) {
            cssstyle = 'display:block';
        } else {
            cssstyle = 'display:none';
        }
        MessageGroupListHtml += '<div id="DivMessageInfo" style="' + cssstyle + '" class="alert alert-warning mtb15" role="alert">You must join this group to post messages or comments.</div>';
        if (MessageGroupList.IsGroupJoined == true) {
            MessageGroupListHtml += '<div class="col-xs-12 mtb20 text-center">\
                                    <button class="btn btn-md btn-success text-uppercase" onclick="postMessage()">Post Message</button>\
                                   </div>' ;
        }
        for (var i = 0; i < MessageGroupList.MessageCenterGroupsModelList.MessageCenterGroupMessageList.length; i++) {
            var clickevent = '';
            var Class = '';
            if (MessageGroupList.MessageCenterGroupsModelList.MessageCenterGroupMessageList[i].IsUserGroupLiked == true) {
                Class = 'fa text-success fa-heart';
            }
            else {
                Class = 'fa fa-heart-o';
                if (MessageGroupList.IsGroupJoined == true) {
                    clickevent = 'onclick = "AddLike(' + MessageGroupList.MessageCenterGroupsModelList.MessageCenterGroupMessageList[i].MsgCenterGroupMsgId + ')"';
                }
            }
            var htmlcomment = '';
            if (MessageGroupList.IsGroupJoined == true) {
                htmlcomment = ' onclick = "AddMsgComments(' + MessageGroupList.MessageCenterGroupsModelList.MessageCenterGroupMessageList[i].MsgCenterGroupMsgId + ')"';
            }
            MessageGroupListHtml +=
                 '<div class="row mtb15" style="border-bottom:1px solid #ddd;">' +
                                '<div class="col-xs-3"><img src="' + MessageGroupList.MessageCenterGroupsModelList.MessageCenterGroupMessageList[i].ProfileImage + '" class="img-thumbnail" width="100%"></div>' +
                                '<div class="col-xs-9 mtb10" style="padding-left:0;">' +
                                    '<div><b class="text-primary"> ' + MessageGroupList.MessageCenterGroupsModelList.MessageCenterGroupMessageList[i].Name + '</b></div>' +
                                    '<div class="text-muted"><small> ' + MessageGroupList.MessageCenterGroupsModelList.MessageCenterGroupMessageList[i].Duration + ' in ' + MessageGroupList.MessageCenterGroupsModelList.MessageCenterGroupMessageList[i].location + '</small></div>' +
                                '</div>' +
                                '<div class="col-xs-12 mtb10">' +
                                    '<div> ' + MessageGroupList.MessageCenterGroupsModelList.MessageCenterGroupMessageList[i].Message + ' </div>' +
                                    '<div class="text-muted mtb10">' +
                                        '<a ' + clickevent + ' id="Add' + MessageGroupList.MessageCenterGroupsModelList.MessageCenterGroupMessageList[i].MsgCenterGroupMsgId + '" ><i class="' + Class + '"></i> <span >' + MessageGroupList.MessageCenterGroupsModelList.MessageCenterGroupMessageList[i].LikeCount + '</span> likes</a>' +
                                        '<span class="plr15"></span>' +
                                        '<a ' + htmlcomment + ' ><i class="fa fa-comment-o"></i> ' + MessageGroupList.MessageCenterGroupsModelList.MessageCenterGroupMessageList[i].CommentCount + ' comments</a>' +
                                    '</div>' +
                                '</div>' +
                                '</div>';
        }
    }
    else {
        if (MessageGroupList.IsGroupJoined == true) {
            MessageGroupListHtml += '<div class="col-xs-12 mtb20 text-center">\
                                    <button class="btn btn-md btn-success text-uppercase" onclick="postMessage()">Post Message</button>\
                                   </div>' ;
        }
        MessageGroupListHtml += '<div class="row ptb15"><div class="col-xs-12 text-center">No data found.</div></div>';
    }
    $('#profile').html(MessageGroupListHtml);
    if (MessageGroupList.MessageCenterGroupsModelList.MessageCenterGroupMembersList.length > 0) {
        var MembersGroupListHtml = '';
        var GuidedSoundFile = "";
        for (var i = 0; i < MessageGroupList.MessageCenterGroupsModelList.MessageCenterGroupMembersList.length; i++) {
            MembersGroupListHtml +=
                   '<div class="row mtb15">' +
                        '<div class="col-xs-3"><img src="' + MessageGroupList.MessageCenterGroupsModelList.MessageCenterGroupMembersList[i].ProfileImage + '" class="img-thumbnail" width="100%"></div>' +
                                '<div class="col-xs-9 mtb10" style="padding-left:0;">' +
                                    '<div><b class="text-primary">' + MessageGroupList.MessageCenterGroupsModelList.MessageCenterGroupMembersList[i].Name + '</b> in ' + MessageGroupList.MessageCenterGroupsModelList.MessageCenterGroupMembersList[i].Location + '</div>' +
                                    '<div class="text-muted"><small>about ' + MessageGroupList.MessageCenterGroupsModelList.MessageCenterGroupMembersList[i].InsertDateTime + ' </small></div>' +
                        '</div>' +
                    '</div>';
        }
    }
    else {
        MembersGroupListHtml = '<div class="row ptb15"><div class="col-xs-12 text-center">No data found.</div></div>';
    }
    $('#messages').html(MembersGroupListHtml);
}
function GetTechSupportByMsgCenterGroupIdCallback(result) {
    var MessageGroupList = JSON.parse(result.Response.Data);
    if (MessageGroupList.length > 0) {
        var MessageGroupListHtml = '';
        for (var i = 0; i < MessageGroupList.length; i++) {
            MessageGroupListHtml +=
                   '<div class="alert alert-success text-center mt5" role="alert">'
                        + MessageGroupList[i].Information +
                    '</div>';
        }
    }
    else {
        MessageGroupListHtml = '<div class="row ptb15"><div class="col-xs-12 text-center">No data found.</div></div>';
    }
    $('#DivAnnounceMent').html(MessageGroupListHtml);
}
function GetCrowdSupportQuestionAnswersCallback(result) {
    var IsUserJoined = window.sessionStorage.getItem('IsGroupJoined');
    var MessageGroupList = JSON.parse(result.Response.Data);
    if (MessageGroupList.length > 0) {
        var MatchedValue = '';
        $('#DivCroudSupport').empty();
        for (var i = 0; i < MessageGroupList.length; i++) {
            var MessageGroupListHtmlExtra = '';
            var count;
            if (MatchedValue == MessageGroupList[i].CrowdSupportQuestionId) {
                MessageGroupListHtmlExtra +=
                     '<hr/>' +
                     '<p align="justify">' + MessageGroupList[i].Answer + '<br><span class="text-success"> -By ' + MessageGroupList[i].AnswerUserName + '<span></p>';
                $('#AddAnswer' + count).append(MessageGroupListHtmlExtra);
            }
            else {
                count = i;
                var MessageGroupListHtml = '';
                MessageGroupListHtml += '<h4> ' + MessageGroupList[i].Question + '</h4>';
                if (MessageGroupList[i].Answer != "") {
                    MessageGroupListHtml += '<p id="AddAnswer' + i + '" align="justify" class="">' + MessageGroupList[i].Answer + '<br><span class="text-success"> -By ' + MessageGroupList[i].AnswerUserName + '<span></p>';
                }
                var Reply = '';
                if (IsUserJoined == 'true') {
                    Reply = 'onclick = "GiveAnswer(' + MessageGroupList[i].CrowdSupportQuestionId + ')"';
                    $('#AddCoudSupport').show();
                }
                else {
                    $('#AddCoudSupport').hide();
                }
                MessageGroupListHtml += '<a class="text-success pull-right" ' + Reply + '  >Reply</a>' +
            '<div class="clearfix"></div>';
                $('#DivCroudSupport').append(MessageGroupListHtml);
                MatchedValue = MessageGroupList[i].CrowdSupportQuestionId;
            }
        }
    }
    else {
        if (IsUserJoined == 'true') {
            $('#AddCoudSupport').show();
        }
        else {
            $('#AddCoudSupport').hide();
        }
        MessageGroupListHtml = '<div class="row ptb15"><div class="col-xs-12 text-center">No data found.</div></div>';
        $('#DivCroudSupport').html(MessageGroupListHtml);
    }
}
function InsertCrowdSupportAnswersCallback(result) {
    if (result.Response.Result) {
        window.location.href = "crowd_support.html";
    }
}
function InsertCrowdSupportQuestionCallback(result) {
    if (result.Response.Result) {
        window.location.href = "crowd_support.html";
    }
}
function InsertCrowdSupportQuestionCallback(result) {
    if (result.Response.Result) {
        window.location.href = "crowd_support.html";
    }
}
function InsertMessageCenterGroupMessageLikesCallback(result) {
    if (result.Response.Result) {
        var MsgCenterGroupMsgId = window.sessionStorage.getItem('MsgCenterGroupMsgId');
        var RsponseValue = JSON.parse(result.Response.Data);
        var htmlval = '<i class="fa text-success fa-heart"></i> ' + RsponseValue + ' likes';
        $("#Add" + MsgCenterGroupMsgId + "").html(htmlval);
        $("#Add" + MsgCenterGroupMsgId + "").removeAttr('onclick');
    }
}
function AddCommentsToGroupMessageCenterCallback(result) {
    if (result.Response.Result) {
        window.location.href = "viewgroups.html";
    }
}
function AddMsgCenterGroupMembersCenterCallback(result) {
    if (result.Response.Result) {
        //$('#btnJoinGroup').hide();
        //$('#DivMessageInfo').hide();
        //$('#btnLeavegroup').show();
        window.location.href = "viewgroups.html";
    }
}
function LeaveGroupByUserIdCallback(result) {
    if (result.Response.Result) {
        //$('#btnJoinGroup').show();
        //$('#DivMessageInfo').show();
        //$('#btnLeavegroup').hide();
        window.location.href = "viewgroups.html";
    }
}
function AddMessageCenterGroupMessageCallback(result) {
    if (result.Response.Result) {
        //$('#btnJoinGroup').show();
        //$('#DivMessageInfo').show();
        //$('#btnLeavegroup').hide();
        window.location.href = "viewgroups.html";
    }
}