﻿function GoogleMap(latlngJson) {
    
    //var latlngJson = [
    //        { lat: "-33.890542", lng: "151.274856" },
    //        { lat: "57.77828", lng: "14.17200" },
    //        { lat: "35.266505", lng: "-80.951879" },
    //        { lat: "38.691093", lng: "-77.160848" },
    //        { lat: "43.601245", lng: "-79.491400" }
    //];

    this.initialize = function () {
        $('#map_canvas').addClass("map_canvas");
        var map = showMap();
        addMarkersToMap(map);
        
    }
    
    var showMap = function () {
        var mapOptions = {
            zoom: 3,
            center: new google.maps.LatLng(42.354183, -71.065063),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }

        var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

        return map;
    }

    var addMarkersToMap = function (map) {

        var mapBounds = new google.maps.LatLngBounds();

        var latitudeAndLongitudeOne = "";//new google.maps.LatLng('-33.890542', '151.274856');
        
        for (var i = 0; i < latlngJson.length; i++) {
            latitudeAndLongitudeOne = new google.maps.LatLng(latlngJson[i].lat, latlngJson[i].lng);
            var markerOne = new google.maps.Marker({
                position: latitudeAndLongitudeOne,
                map: map
            });

            if (userData.Latitude == latlngJson[i].lat && userData.Longitude == latlngJson[i].lng) {
                // Add circle overlay and bind to marker
                var circle = new google.maps.Circle({
                    map: map,
                    radius: 250093,
                    fillColor: '#AA0000'
                });
                circle.bindTo('center', markerOne, 'position');
            }

            mapBounds.extend(latitudeAndLongitudeOne);
        }

        //var latitudeAndLongitudeTwo = new google.maps.LatLng('57.77828', '14.17200');

        //var markerOne = new google.maps.Marker({
        //    position: latitudeAndLongitudeTwo,
        //    map: map            
        //});

        
        //mapBounds.extend(latitudeAndLongitudeTwo);

        map.fitBounds(mapBounds);

        $('#map_canvas').removeClass("map_canvas");
    }
}