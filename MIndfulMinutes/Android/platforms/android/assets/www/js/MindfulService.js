﻿/* Global Variables*/

var servicePath = "http://192.168.1.79/MindfulMinutesService/MindfulMinutesService.svc/";
//var servicePath = "http://localhost:49277/MindfulMinutesService.svc/";
//var servicePath = "http://ec2-54-200-164-172.us-west-2.compute.amazonaws.com/mindfulminuteservice/MindfulMinutesService.svc/";

//Declare Varibales which are using in AJAX method.
var Type = "POST";
var Url = servicePath;
var Data = "";
var ContentType = "application/json;charset=utf-8";
var DataType = "json";
var crossDomain = true;
var isSuccess = false;
var callname = "";

//Call MindfulMinutes Service
function callMindfulService(data, callname) {

    $("#dvProcessing").show();
    Url = servicePath + callname;

    $.ajax({
        type: Type,
        url: Url,
        data: data,
        contentType: ContentType,
        dataType: DataType,
        crossDomain: crossDomain,
        success: function (result) {
            $("#dvProcessing").hide();
            if (result.Response.Result) {
                ServiceSucceeded(result, callname);
            } else {
                ServiceFailed(result);
            }
        },
        error: function (jqXHR, exception) {
            $("#dvProcessing").hide();
            //alert('Internal error: ' + jqXHR.responseText);

            //ServiceFailed(result);     
            var errormsg = "";

            if (jqXHR.status == 0) {
                errormsg = 'Not connected.\n Verify Your Internet Connection.';
            } else if (jqXHR.status == 404) {
                errormsg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                errormsg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                errormsg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                errormsg = 'Time out error.';
            } else if (exception === 'abort') {
                errormsg = 'Ajax request aborted.';
            } else {
                errormsg = ('Uncaught Error.\n' + jqXHR.responseText);
            }

            showAlert(errormsg, "danger");
        }
    });
}

function ServiceFailed(result) {
    //alert(result.Response.Message);
    showAlert(result.Response.Message, "danger");
}

function ServiceSucceeded(result, callname) {
    switch (callname) {
        case "SignUp":
            SignUpCallback(result);
            break;
        case "SignIn":
            SignInCallback(result);
            break;
        case "GetMemberProfile":
            GetProfileCallback(result);
            break;
        case "EditProfilePicture":
            EditPictureCallback(result);
            break;
        case "EditMemberProfile":
            EditProfileCallback(result);
            break;
        case "GetUserSettings":
            GetUserSettingsCallback(result);
            break;
        case "EditUserSettings":
            SaveUserSettingsCallback(result);
            break;
        case "ForgotPassword":
            RequestPasswordCallback(result);
            break;
        case "GetAmbientSoundList":
            GetAmbientSoundCallback(result);
            break;
        case "AddPresetTimer":
            SavePresetTimerCallback(result);
            break;
        case "GetPresetTimerList":
            GetPresetTimerListCallback(result);
            break;
        case "GetPresetTimerByPresetId":
            GetPresetTimerCallback(result);
            break;
        case "DeletePresetTimer":
            DeletePresetTimerCallback(result);
            break;
        case "AddMeditationStats":
            AddMeditationStatsCallback(result);
            break;
        case "GetAllUsersLocation":
            GetAllUsersLocationCallback(result);
            break;
        case "GetNeurominutesFreeSamplerList":
            GetFreeSamplerCallback(result);
            break;
        case "IncreaseNeurominutesFreeSamplerViewCount":
            IncreaseViewCountCallback(result);
            break;
        case "GetNeurominutesTrackList":
            GetNeurominTrackCallback(result);
            break;
        case "GetNeurominutesTrackDetailListByTrackId":
            GetNeurominTrackDetailCallback(result);
            break;
        case "AddNeurominReview":
            AddNeurominReviewCallback(result);
            break;
        case "LikeUnlikeTrack":
            if ($('#iLikeUnlike').hasClass("text-success"))
                $('#iLikeUnlike').removeClass("text-success");
            else
                $('#iLikeUnlike').addClass("text-success");
            break;
        case "GetAllGuidedMeditationCategory":
            GetAllGuidedMeditationCategoryCallback(result);
            break;
        case "GetAllGuidedUploadMeditationByCategory":
            GetAllGuidedUploadMeditationByCategoryCallback(result);
            break;
        case "GetGuidedUploadMeditationDetails":
            GetGuidedUploadMeditationDetailsCallback(result);
            break;
        case "UpdatePlayCount":
            UpdatePlayCountCallback(result);
            break;
        case "AddNeurominReview":
            AddNeurominReviewCallback(result);
            break;
        case "ManageUserRating":
            ManageUserRatingCallback(result);
            break;
        case "GetUserRating":
            GetUserRatingCallback(result);
            break;
        case "GetRelatedGuidedUploadMeditation":
            GetRelatedGuidedUploadMeditationCallback(result);
            break;
        case "InsertSubscriptionForApplication":
            if (result.Response.Result) {
                var userData = JSON.parse(window.localStorage.getItem("userData"));
                userData.IsFullPurchased = true;
                window.localStorage.setItem("userData", JSON.stringify(userData));
                $('#DivFullSubscription').hide();
            }            
            showAlert(result.Response.Message, "info");
            $('.modal').modal('hide');
            break;
        case "InsertSubscriptionForModule":
            if (result.Response.Result) {
                $('#aPricingPlan').hide();
            }
            showAlert(result.Response.Message, "info");
            $('.modal').modal('hide');
            break;
        case "InsertSubscriptionForProduct":
            //if (result.Response.Result)
            //    $('#aPricingPlan').hide();
            showAlert(result.Response.Message, "info");
            $('.modal').modal('hide');
            window.location = window.location;
            break;
        case "GetAllGuidedMeditationCategoryDropDown":
            GetAllGuidedMeditationCategoryDropDownCallback(result);
            break;
        case "InsertGuidedMeditationData":
            InsertGuidedMeditationDataCallback(result);
            break;
        case "InsertUpdateMyUploads":
            InsertUpdateMyUploadsCallback(result);
            break;
        case "GetAllMyUpload":
            GetAllMyUploadCallback(result);
            break;
        case "UpdateMyUploadMeditationPlayCount":
            UpdateMyUploadMeditationPlayCountCallback(result);
            break;
        case "GetGuidedUploadMeditationMediaList":
            GetGuidedUploadMeditationMediaListCallback(result);
            break;
        case "UpdateMyUploadMeditationMediaPlayCount":
            UpdateMyUploadMeditationMediaPlayCountCallback(result);
            break;
        case "CheckForModuleSubscription":
            CheckForModuleSubscriptionCallback(result);
            break;
        case "AddGuidedMeditationStats":
            AddGuidedMeditationStatsCallback(result);
            break;
        case "GetTalkInterviewList":
            GetTalkInterviewListCallback(result);
            break;
        case "UpdateTalksAndInterviewsPlayCount":
            UpdateTalksAndInterviewsPlayCountCallback(result);
            break;
        case "GetUserUploadWritingsList":
            GetDivWritingsListCallback(result);
            break;
        case "UpadateUserUploadWritingsViewCount":
            UpdateWritingsViewCountCallback(result);
            break;
        case "GetUserUploadResourcesList":
            GetDivUserUploadResourcesListCallback(result);
            break;
        case "UpadateUserUploadResourcesViewCount":
            UpdateUserUploadResourcesViewCountCallback(result);
            break;
        case "UserUploadResourcesVideo":
            UserUploadResourcesVideoCallback(result);
            break;
        case "GetResourceGroupList":
            GetResourceGroupListCallback(result);
            break;
        case "AddAppReview":
            location.href = ""; //store url scheme
            break;
        case "UpdateUserPayPalEmail":
            var userData = JSON.parse(window.localStorage.getItem("userData"));
            userData.PaypalEmail = $('#txtEmail').val().trim();
            window.localStorage.setItem("userData", JSON.stringify(userData));
            window.location.href = 'myuploads.html';
            break;
        case "GetRecipientList":
            GetRecipientListCallback(result);
            break;
        case "GetResourceGroupDetails":
            GetResourceGroupDetailsCallback(result);
            break;
        case "AddRemoveMemberFromGroup":
            AddRemoveMemberFromGroupCallback(result);
            break;
        case "AddMessageToGroup":
            window.location.href = "premium_groups_view.html";
            break;
        case "AddLikesToGroupMessage":
            AddLikesToGroupMessageCallback(result);
            break;
        case "AddCommentsToGroupMessage":
            window.location.href = "premium_groups_view.html";
            break;
        case "GetResourceMessageComments":
            GetResourceMessageCommentsCallback(result);
            break;
        case "UpdateGroupSettings":
            UpdateGroupSettingsCallback(result);
            break;
        case "SendInvitationToJoinGroup":
            SendInvitationToJoinGroupCallback(result);
            break;
case "GetMessageGroupList":
            GetMessageGroupListCallback(result);
            break;
        case "GetMsgCenterGroupById":
            GetMsgCenterGroupByIdCallback(result);
            break;
        case "GetTechSupportByMsgCenterGroupId":
            GetTechSupportByMsgCenterGroupIdCallback(result);
            break;
        case "GetCrowdSupportQuestionAnswers":
            GetCrowdSupportQuestionAnswersCallback(result);
            break;
        case "InsertCrowdSupportAnswers":
            InsertCrowdSupportAnswersCallback(result);
            break;
        case "InsertCrowdSupportQuestion":
            InsertCrowdSupportQuestionCallback(result);
            break;
        case "InsertMessageCenterGroupMessageLikes":
            InsertMessageCenterGroupMessageLikesCallback(result);
            break;
        case "AddCommentsToGroupMessageCenter":
            AddCommentsToGroupMessageCenterCallback(result);
            break;
        case "AddMsgCenterGroupMembers":
            AddMsgCenterGroupMembersCenterCallback(result);
            break;
        case "LeaveGroupByUserId":
            LeaveGroupByUserIdCallback(result);
            break;
        case "GetMsgCenterGroupComments":
            GetMsgCenterGroupCommentsCallback(result);
            break;
        case "AddMessageCenterGroupMessage":
            AddMessageCenterGroupMessageCallback(result);
            break;
        default:
    }
}
