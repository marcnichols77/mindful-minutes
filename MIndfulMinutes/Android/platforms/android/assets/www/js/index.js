/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function () {        
        //alert("onDeviceReady event");
        
        var _deviceInfo = window.localStorage.getItem("deviceInfo") != null ?
                            JSON.parse(window.localStorage.getItem("deviceInfo")) :
                            { token: "", platform: "" };
        
        _deviceInfo.platform = device.platform;
        window.localStorage.setItem("deviceInfo", JSON.stringify(_deviceInfo));
        
        var push = PushNotification.init({
            "android": {
                "senderID": "843305866319",
                "icon": "icon"
                //"iconColor": "#dddddd"
                , "forceShow": true
                , clearNotifications: false
            },
            "ios": { "alert": "true", "badge": "true", "sound": "true" },
            "windows": {}
        });
        push.on('registration', function (data) {
            //alert("registration event");
            console.log(JSON.stringify(data));
            console.log(data.registrationId);
            
                _deviceInfo.token = data.registrationId;
            //alert("token : "+ data.registrationId+"\n platform : "+ device.platform );
            
            window.localStorage.setItem("deviceInfo", JSON.stringify(_deviceInfo));
            
        });
        push.on('notification', function (data) {
            console.log("notification event");
            //alert(JSON.stringify(data));

            if (data.additionalData.MessageFor.trim() == "group") {
                window.sessionStorage.setItem("GroupId", GroupId);
                window.location.href = "premium_groups_view.html";
            }
                        
            push.finish(function () {
                console.log('finish successfully called');
            });
        });
        push.on('error', function (e) {
            console.log("push error");
        });

        //navigator.geolocation.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true });

        //app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

app.initialize();

function onError(error) {
    alert('code: ' + error.code + '\n' +
          'message: ' + error.message + '\n');
}

function onSuccess(position) {
    //alert(position.coords.latitude);
    //alert(position.coords.longitude);

    var _deviceLocations = "";

    if (window.sessionStorage.getItem("deviceLocations") != null) {
        _deviceLocations = JSON.parse(window.sessionStorage.getItem("deviceLocations"));

        _deviceLocations.lat = position.coords.latitude;
        _deviceLocations.lng = position.coords.longitude;

        window.sessionStorage.setItem("deviceLocations", JSON.stringify(_deviceLocations));
    } else {
        _deviceLocations = { lat: position.coords.latitude, lng: position.coords.longitude };

        window.sessionStorage.setItem("deviceLocations", JSON.stringify(_deviceLocations));
    }

    //alert(JSON.stringify(_deviceLocations));
}