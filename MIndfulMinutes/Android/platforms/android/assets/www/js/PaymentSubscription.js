
var app = {};


app.initialize = function (ps) {

    this.ps = ps;
    document.addEventListener('deviceready', this.bind(this.onDeviceReady), false);
};

app.onDeviceReady = function () {

    this.initStore();
};


app.initStore = function () {

    if (!window.store) {
        alert('Store not available')
        return;
    }
    store.verbosity = store.DEBUG;
    
    
  //subscribed Start
    store.register({
        id: this.ps, // id without package name!
        alias: 'subscription1',
        type: store.PAID_SUBSCRIPTION
    });

    store.when("product").updated(function (p) {
  
       // app.renderIAP(p);
    });

    store.when("subscription1").approved(function (p) {
    
       // log("verify subscription");
        p.verify();
    });
    store.when("subscription1").verified(function (p) {
        //log("subscription verified");
        p.finish();
    });
    store.when("subscription1").unverified(function (p) {
        //log("subscription unverified");
    });
    store.when("subscription1").updated(function (p) {
        if (p.owned) {
             alert( 'You are a lucky subscriber!')
        }
        else {
            alert( 'You are not subscribed');
        }
    });
	//subscribed END

    store.error(function (error) {
        alert(error.code)
        alert(error.message)        
    });

    store.when("extra life").approved(function (order) {
        order.finish();
       
    });


    store.ready(function () {

    });


    store.ready(function () {

    });

    store.refresh();
    store.order(this.ps);
};



// make sure fn will be called with app as 'this'
app.bind = function (fn) {
    return function () {
        fn.call(app, arguments);
    };
};
 
