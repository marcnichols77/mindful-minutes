
var app = {};


app.initialize = function (ps) {

    this.ps = ps;
    document.addEventListener('deviceready', this.bind(this.onDeviceReady), false);
};

app.onDeviceReady = function () {

    this.initStore();
};


app.initStore = function () {

    if (!window.store) {
        alert('Store not available')
        return;
    }
    store.verbosity = store.DEBUG;
    store.register({
        id: this.ps,
        alias: 'extra life',
        type: store.CONSUMABLE
    });

    store.when("product").updated(function (p) {
 
        //app.renderIAP(p);
    });

    store.error(function (error) {
        //alert(error.code)
        //alert(error.message)

        InsertSubscriptionErrorLog(error.code, error.message);

    });

    store.when("extra life").approved(function (order) {
        order.finish();
        Subscription();
    });


    store.ready(function () {

    });


    store.ready(function () {

    });

    store.refresh();
    store.order(this.ps);
};



// make sure fn will be called with app as 'this'
app.bind = function (fn) {
    return function () {
        fn.call(app, arguments);
    };
};

function SubscribeForFullAppication(PaymentKey)
{
    window.localStorage.setItem("SubscriptionType", "Application");
    app.initialize(PaymentKey);
}

function SubscribeForFullModule(ModuleName, PaymentKey) {
    window.localStorage.setItem("SubscriptionType", "Module");
    window.localStorage.setItem("ModuleName", ModuleName);
    app.initialize(PaymentKey);
}

function SubscribeForFullProduct(ModuleName, SectionName, ProductID, PaymentKey, ProductName) {
    window.localStorage.setItem("SubscriptionType", "Product");
    window.localStorage.setItem("ModuleName", ModuleName);
    window.localStorage.setItem("SectionName", SectionName);
    window.localStorage.setItem("ProductID", ProductID);
    window.localStorage.setItem("ProductName", ProductName);
    app.initialize(PaymentKey);
}

function Subscription()
{
    var SubscriptionType = window.localStorage.getItem("SubscriptionType");
    window.localStorage.removeItem("SubscriptionType");

    if (SubscriptionType === "Application") {

        InsertSubscriptionForApplication();
    }
    else if (SubscriptionType == "Module") {
        var ModuleName = window.localStorage.getItem("ModuleName");
        window.localStorage.removeItem("ModuleName");

        InsertSubscriptionForModule(ModuleName);
    }
    else if (SubscriptionType == "Product") {
        var ModuleName = window.localStorage.getItem("ModuleName");
        window.localStorage.removeItem("ModuleName");
        var SectionName = window.localStorage.getItem("SectionName");
        window.localStorage.removeItem("SectionName");
        var ProductID = window.localStorage.getItem("ProductID");
        window.localStorage.removeItem("ProductID");
        var ProductName = window.localStorage.getItem("ProductName");
        window.localStorage.removeItem("ProductName");

        InsertSubscriptionForProduct(ModuleName, SectionName, ProductID, ProductName);
    }
}

function InsertSubscriptionForApplication()
{
    var userData = JSON.parse(window.localStorage.getItem("userData"));
    Data = {
        "UserID": userData.Id
    };

    callname = "InsertSubscriptionForApplication";
    callMindfulService(JSON.stringify(Data), callname);
}

function InsertSubscriptionForModule(ModuleName) {
    var userData = JSON.parse(window.localStorage.getItem("userData"));
    Data = {
        "UserID": userData.Id,
        "ModuleName": ModuleName
    };

    callname = "InsertSubscriptionForModule";
    callMindfulService(JSON.stringify(Data), callname);
}

function InsertSubscriptionForProduct(ModuleName, SectionName, ProductId, ProductName) {
    var userData = JSON.parse(window.localStorage.getItem("userData"));
    Data = {
        "UserID": userData.Id,
        "ModuleName": ModuleName,
        "SectionName": SectionName,
        "ProductId": ProductId,
        "ProductName": ProductName
    };

    callname = "InsertSubscriptionForProduct";
    callMindfulService(JSON.stringify(Data), callname);
}

function InsertSubscriptionErrorLog(ErrorCode,ErrorMessage) {
    var userData = JSON.parse(window.localStorage.getItem("userData"));

    var ModuleName = "";
    if (window.localStorage.getItem("ModuleName") != null) {
        ModuleName = window.localStorage.getItem("ModuleName");
        window.localStorage.removeItem("ModuleName");
    }    

    var SectionName = "";
    if (window.localStorage.getItem("SectionName") != null) {
        SectionName = window.localStorage.getItem("SectionName");
        window.localStorage.removeItem("SectionName");
    }

    var ProductName = "";
    if (window.localStorage.getItem("ProductName") != null) {
        ProductName = window.localStorage.getItem("ProductName");
        window.localStorage.removeItem("ProductName");
    }

    Data = {
        "UserId": userData.Id,
        "ModuleName": ModuleName,
        "SectionName": SectionName,
        "ProductName": ProductName,
        "ErrorCode": ErrorCode,
        "ErrorMessage": ErrorMessage
    };

    callname = "InsertSubscriptionErrorLog";
    callMindfulService(JSON.stringify(Data), callname);
}
